package ru.mirsaitov.tm.core;

import ru.mirsaitov.tm.core.constant.TerminalConst;
import ru.mirsaitov.tm.core.entity.Command;
import ru.mirsaitov.tm.core.listeners.SystemListener;
import ru.mirsaitov.tm.core.listeners.UserListener;
import ru.mirsaitov.tm.core.observer.Publisher;
import ru.mirsaitov.tm.core.observer.PublisherImpl;
import ru.mirsaitov.tm.core.service.ScheduledTaskService;

/**
 * Task-manager application
 *
 * @author Mirsaitov Grigorii
 */
public class Application {

    /**
     * Entry point of programm
     */
    public static void main(final String[] args) {
        ScheduledTaskService service = Context.getInstance().getSheduledTaskService();
        service.start();
        Publisher publisher = new PublisherImpl();
        publisher.subscribe(new SystemListener());
        publisher.subscribe(Context.getInstance().getProjectListener());
        publisher.subscribe(Context.getInstance().getTaskListener());
        publisher.subscribe(new UserListener());
        publisher.notifySubscribers(new Command(TerminalConst.CMD_WELCOME, null));
        publisher.run();
    }

}