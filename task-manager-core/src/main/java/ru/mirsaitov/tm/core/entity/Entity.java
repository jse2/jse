package ru.mirsaitov.tm.core.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public abstract class Entity {

    protected Long id = System.nanoTime();

    protected Long userId;

}
