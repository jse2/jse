package ru.mirsaitov.tm.core.repository;

import ru.mirsaitov.tm.core.entity.Entity;
import ru.mirsaitov.tm.core.enumerated.FileExtension;
import ru.mirsaitov.tm.core.service.FileService;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public abstract class Repository<T extends Entity> {

    protected final List<T> items = new CopyOnWriteArrayList<>();

    private final FileService fileService = FileService.getInstance();

    protected final String fileName;

    protected Repository(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Return items by userId
     *
     * @param userId
     */
    public List<T> findAll(final Long userId) {
        return items.stream()
                .filter(item -> Objects.equals(item.getUserId(), userId))
                .collect(Collectors.toList());
    }

    /**
     * Return all items
     *
     */
    public List<T> findAll() {
        return new LinkedList<>(items);
    }

    /**
     * Delete item from structures
     *
     * @param item
     */
    public void delete(final T item) {
        items.remove(item);
    }

    /**
     * Delete items from structures
     *
     * @param deleteItems
     */
    public void deleteAll(final List<T> deleteItems) {
        items.removeAll(deleteItems);
    }

    /**
     * Save item
     *
     * @param item
     */
    public void save(final T item) {
        if (!items.contains(item)) {
            items.add(item);
        }
    }

    /**
     * Size of repository
     *
     * @param userId
     */
    public int size(final Long userId) {
        return findAll(userId).size();
    }

    /**
     * Save elements to file
     *
     * @param userId
     * @param extension
     */
    public void saveToFile(final Long userId, final FileExtension extension) {
        switch (extension) {
            case JSON:
                fileService.writeListJson(userId, fileName, findAll(userId));
                break;
            case XML:
                fileService.writeListXml(userId, fileName, findAll(userId));
                break;
            default:
                break;
        }
    }

    /**
     * Read projects from file
     *
     * @param userId
     * @param extension
     */
    protected List<T> readFromFile(final Long userId, final FileExtension extension, final Class cl) {
        switch (extension) {
            case JSON:
                return fileService.readListJson(userId, fileName, cl);
            case XML:
                return fileService.readListXml(userId, fileName, cl);
            default:
                return Collections.EMPTY_LIST;
        }
    }

}
