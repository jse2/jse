package ru.mirsaitov.tm.core.exception;

public class NotExistElementException extends Exception {

    public NotExistElementException(final String message) {
        super(message);
    }

}
