package ru.mirsaitov.tm.core.service;

import ru.mirsaitov.tm.core.enumerated.Role;
import ru.mirsaitov.tm.core.exception.DuplicateLoginException;
import ru.mirsaitov.tm.core.repository.UserRepository;
import ru.mirsaitov.tm.core.entity.User;
import ru.mirsaitov.tm.core.util.HashUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

public class UserService {

    private static final Logger logger = LogManager.getLogger(UserService.class);

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> create(final String login, final String password) throws DuplicateLoginException {
        if (login == null || login.isBlank()) {
            return Optional.empty();
        }
        if (password == null || password.isBlank()) {
            return Optional.empty();
        }
        var user = findByLogin(login);
        if (user.isPresent()) {
            final String errorMsg = String.format(bundle.getString("dublicateLoginException"), login);
            logger.error(errorMsg, login);
            throw new DuplicateLoginException(errorMsg);
        }
        return Optional.of(userRepository.create(login, HashUtil.hashMD5(password)));
    }

    public Optional<User> create(final String login, final String password, final Role role) throws DuplicateLoginException {
        if (role == null) {
            return Optional.empty();
        }
        return this.create(login, password).map(user -> {
            user.setRole(role);
            return user;
        });
    }

    public void clear() {
        userRepository.clear();
    }

    public Optional<User> findByLogin(final String login) {
        if (login == null || login.isBlank()) {
            return Optional.empty();
        }
        return userRepository.findByLogin(login);
    }

    public Optional<User> findById(final Long id) {
        if (id == null) {
            return Optional.empty();
        }
        return userRepository.findById(id);
    }

    public Optional<User> findByIndex(int index) {
        if (index < 0 || index >= userRepository.getSize()) {
            return Optional.empty();
        }
        return userRepository.findByIndex(index);
    }

    public Optional<User> removeByLogin(final String login) {
        if (login == null || login.isBlank()) {
            return Optional.empty();
        }
        return userRepository.removeByLogin(login);
    }

    public Optional<User> removeById(final Long id) {
        if (id == null) {
            return Optional.empty();
        }
        return userRepository.removeById(id);
    }

    public Optional<User> removeByIndex(int index) {
        if (index < 0 || index >= userRepository.getSize()) {
            return Optional.empty();
        }
        return userRepository.removeByIndex(index);
    }

    public Optional<User> updateById(User user) {
        if (user.getId() == null) {
            return Optional.empty();
        }
        return userRepository.updateById(user);
    }

    public Optional<User> updateByLogin(User user) {
        if (user.getLogin() == null || user.getLogin().isBlank()) {
            return Optional.empty();
        }
        return userRepository.updateByLogin(user);
    }

    public Optional<User> updateByIndex(final int index, final User user) {
        if (index < 0 || index >= userRepository.getSize()) {
            return Optional.empty();
        }
        return userRepository.updateByIndex(index, user);
    }

    public Optional<User> authentication(final String login, final String password) {
        if (login == null || login.isBlank()) {
            return Optional.empty();
        }
        if (password == null || password.isBlank()) {
            return Optional.empty();
        }
        return userRepository.findByLogin(login)
                .filter(user -> Objects.equals(HashUtil.hashMD5(password), user.getPassword()));
    }

    public Optional<User> changePassword(final String password, final Long userId) {
        if (password == null || password.isBlank()) {
            return Optional.empty();
        }
        if (userId == null) {
            return Optional.empty();
        }
        return userRepository.findById(userId)
                .map(user -> {
                    user.setPassword(HashUtil.hashMD5(password));
                    return user;
                });
    }

    public Optional<User> updateProfile(
            final Long userId, final String firstName,
            final String middleName, final String lastName
    ) {
        if (userId == null) {
            return Optional.empty();
        }
        if (firstName == null || firstName.isBlank()) {
            return Optional.empty();
        }
        if (middleName == null || middleName.isEmpty()) {
            return Optional.empty();
        }
        if (lastName == null || lastName.isEmpty()) {
            return Optional.empty();
        }
        return userRepository.findById(userId)
                .map(user -> {
                    user.setFirstName(firstName);
                    user.setMiddleName(middleName);
                    user.setLastName(lastName);
                    return user;
                });
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
