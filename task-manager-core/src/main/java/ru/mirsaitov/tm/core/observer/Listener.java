package ru.mirsaitov.tm.core.observer;

import ru.mirsaitov.tm.core.entity.Command;

public interface Listener {

    void execute(Command command);

}