package ru.mirsaitov.tm.core.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.mirsaitov.tm.core.enumerated.Role;

@Getter
@Setter
@Accessors(chain = true)
public class User {

    private Long id = System.nanoTime();

    private String login = "";

    private String firstName = "";

    private String middleName = "";

    private String lastName = "";

    private String password = "";

    private Role role = Role.USER;

    public User() {
    }

    public User(final String login, final String password) {
        this.login = login;
        this.password = password;
    }

    public User(final String login, final String password, final Role role) {
        this(login, password);
        this.role = role;
    }

}
