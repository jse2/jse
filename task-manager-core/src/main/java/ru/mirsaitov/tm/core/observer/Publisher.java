package ru.mirsaitov.tm.core.observer;

import ru.mirsaitov.tm.core.entity.Command;

public interface Publisher {

    public void run();

    void subscribe(Listener listener);

    void unsubscribe(Listener listener);

    void notifySubscribers(Command command);

}