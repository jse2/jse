package ru.mirsaitov.tm.core.listeners;

import java.util.ResourceBundle;

public abstract class AbstractListener {

    protected final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

}
