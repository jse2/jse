package ru.mirsaitov.tm.core.enumerated;

import ru.mirsaitov.tm.core.constant.TerminalConst;

public enum FileExtension {
    JSON(".json"),
    XML(".xml");

    private String extension;

    FileExtension(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

    public static FileExtension getByOption(String option) {
        switch (option) {
            case TerminalConst.OPTION_JSON:
                return FileExtension.JSON;
            case TerminalConst.OPTION_XML:
                return FileExtension.XML;
            default:
                return null;
        }
    }

}
