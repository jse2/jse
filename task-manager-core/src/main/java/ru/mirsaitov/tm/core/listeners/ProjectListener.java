package ru.mirsaitov.tm.core.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.mirsaitov.tm.core.constant.TerminalConst;
import ru.mirsaitov.tm.core.entity.Command;
import ru.mirsaitov.tm.core.entity.Project;
import ru.mirsaitov.tm.core.enumerated.FileExtension;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.observer.Listener;
import ru.mirsaitov.tm.core.service.ProjectService;
import ru.mirsaitov.tm.core.service.ProjectTaskService;
import ru.mirsaitov.tm.core.service.SessionService;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ProjectListener extends AbstractListener implements Listener {

    private static final Logger logger = LogManager.getLogger(ProjectListener.class);

    private final ProjectService projectService;

    private final SessionService sessionService;

    private final ProjectTaskService projectTaskService;

    public ProjectListener(ProjectService projectService, SessionService sessionService, ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void execute(Command command) {
        try {
            switch (command.getCommand()) {
                case TerminalConst.PROJECT_CREATE:
                    createProject(command);
                    break;
                case TerminalConst.PROJECT_CLEAR:
                    clearProject();
                    break;
                case TerminalConst.PROJECT_LIST:
                    listProject(command);
                    break;
                case TerminalConst.PROJECT_VIEW:
                    viewProject(command);
                    break;
                case TerminalConst.PROJECT_UPDATE:
                    updateProject(command);
                    break;
                case TerminalConst.PROJECT_REMOVE:
                    displayProject(removeProject(command));
                    break;
                case TerminalConst.PROJECT_SAVE:
                    saveProjects(command);
                    break;
                case TerminalConst.PROJECT_READ:
                    readProjects(command);
                    break;
                default:
                    break;
            }
        } catch (NotExistElementException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Add task to project
     *
     * @param command - command
     */
    public Project removeProject(final Command command) throws NotExistElementException {
        final Long userId = sessionService.getUserId();
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        final String deleteTask = command.getArgumentByIndex(2);

        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return null;
        }

        boolean delete = deleteTask == null || deleteTask.isEmpty() ? false : deleteTask.equals("task");
        return projectTaskService.removeProject(option, param, delete, userId);
    }

    /**
     * Create project
     *
     * @param command - command
     */
    public void createProject(final Command command) {
        final String name = command.getArgumentByIndex(0);
        final String description = command.getArgumentByIndex(1);
        final Long userId = sessionService.getUserId();
        if (description == null) {
            projectService.create(name, userId);
        } else {
            projectService.create(name, description, userId);
        }
        logger.info(bundle.getString("projectCreate"));
    }

    /**
     * Clear projects
     */
    public void clearProject() {
        projectService.clear(sessionService.getUserId());
        logger.info(bundle.getString("projectClear"));
    }

    /**
     * View project
     *
     * @param command - command
     */
    public void viewProject(final Command command) {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        Optional<Project> project = Optional.empty();
        final Long userId = sessionService.getUserId();
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectService.findByIndex(Integer.parseInt(param) - 1, userId);
                break;
            case TerminalConst.OPTION_NAME:
                project = projectService.findByName(param, userId);
                break;
            case TerminalConst.OPTION_ID:
                project = projectService.findById(Long.parseLong(param), userId);
                break;
        }
        project.ifPresent(this::displayProject);
    }

    /**
     * Display project
     *
     * @param project project
     */
    public void displayProject(Project project) {
        if (project == null) {
            logger.info(bundle.getString("notFound"));
            return;
        }

        logger.info("ID: {}", project.getId());
        logger.info("NAME: {}", project.getName());
        logger.info("DESCRIPTION: {}", project.getDescription());
    }

    /**
     * Update project
     *
     * @param - command
     */
    public void updateProject(final Command command) throws NotExistElementException {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        final String name = command.getArgumentByIndex(2);
        final String description = command.getArgumentByIndex(3);
        if (option == null || param == null || name == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        final Long userId = sessionService.getUserId();
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                if (description == null) {
                    project = projectService.updateByIndex(Integer.parseInt(param) - 1, name, userId);
                } else {
                    project = projectService.updateByIndex(Integer.parseInt(param) - 1, name, description, userId);
                }
                break;
            case TerminalConst.OPTION_ID:
                project = projectService.updateById((Project)new Project()
                        .setDescription(description)
                        .setName(name)
                        .setId(Long.parseLong(param)), userId);
                break;
        }
        displayProject(project);
    }

    /**
     * List projects
     *
     * @param command - command
     */
    public void listProject(final Command command) {
        String option = command.getArgumentByIndex(0);
        if (option == null) {
            option = "";
        }

        List<Project> projects;
        final Long userId = sessionService.getUserId();
        switch (option) {
            case TerminalConst.OPTION_NAME:
                projects = projectService.findAll(userId, Comparator.comparing(Project::getName));
                break;
            case TerminalConst.OPTION_ID:
                projects = projectService.findAll(userId, Comparator.comparing(Project::getId));
                break;
            default:
                projects = projectService.findAll(userId);
                break;
        }
        int index = 1;
        for (final Project project : projects) {
            logger.info("INDEX: {} ID: {} NAME: {} DESCRIPTION: {}", index++, project.getId(), project.getName(), project.getDescription());
        }
    }


    /**
     * Save projects to file
     *
     * @param command - command
     */
    public void saveProjects(final Command command) {
        final String option = command.getArgumentByIndex(0);
        final Long userId = sessionService.getUserId();
        if (option == null || userId == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }

        switch (option) {
            case TerminalConst.OPTION_JSON:
                projectService.saveProjects(userId, FileExtension.JSON);
                break;
            case TerminalConst.OPTION_XML:
                projectService.saveProjects(userId, FileExtension.XML);
                break;
            default:
                break;
        }
    }

    /**
     * Read projects from file
     *
     * @param command - command
     */
    public void readProjects(final Command command) {
        final String option = command.getArgumentByIndex(0);
        final Long userId = sessionService.getUserId();
        if (option == null || userId == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        projectService.readProjects(userId, FileExtension.getByOption(option));
    }

}
