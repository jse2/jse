package ru.mirsaitov.tm.core.repository;

import ru.mirsaitov.tm.core.entity.Project;
import ru.mirsaitov.tm.core.enumerated.FileExtension;
import java.util.*;

public class ProjectRepository extends Repository<Project> {

    private static volatile ProjectRepository instance;

    private final HashMap<String, HashSet<Project>> mapProjects = new HashMap<>();

    private ProjectRepository() {
        super("projects");
    }

    public static ProjectRepository getInstance() {
        ProjectRepository result = instance;
        if (result != null) {
            return result;
        }
        synchronized(ProjectRepository.class) {
            if (instance == null) {
                instance = new ProjectRepository();
            }
            return instance;
        }
    }

    /**
     * Create project by name
     *
     * @param name - name of project
     * @param userId
     * @return created project
     */
    public Project create(final String name, final Long userId) {
        final Project project = new Project(name, userId);
        saveToStructures(project);
        return project;
    }

    private void saveToStructures(Project project) {
        save(project);
        addHashMap(project);
    }

    /**
     * Create project by name and description
     *
     * @param name        - name of project
     * @param description - description of project
     * @param userId
     * @return created project
     */
    public Project create(final String name, final String description, final Long userId) {
        final Project project = create(name, userId);
        project.setDescription(description);
        return project;
    }

    /**
     * Add project to hashmap
     *
     * @param project - project
     */
    private void addHashMap(final Project project) {
        final String name = project.getName();
        var projectsSet = mapProjects.get(name);
        if (projectsSet == null) {
            projectsSet = new HashSet<>();
            projectsSet.add(project);
            mapProjects.put(name, projectsSet);
        } else {
            projectsSet.add(project);
        }
    }

    /**
     * Remove project from hashmap
     *
     * @param project - project
     */
    private void removeFromHashMap(final Project project) {
        var setProjects = mapProjects.get(project.getName());
        if (setProjects != null) {
            setProjects.remove(project);
        }
    }

    /**
     * Clear projects
     *
     * @param userId
     */
    public void clear(final Long userId) {
        final var removeProjects = findAll(userId);
        deleteAll(removeProjects);
        removeProjects.forEach(this::removeFromHashMap);
    }

    /**
     * Find project by index
     *
     * @param index index of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> findByIndex(final int index, final Long userId) {
        var projects = findAll(userId);
        if (index >= 0 && index < projects.size()) {
            return Optional.of(projects.get(index));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Find project by name
     *
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> findByName(final String name, final Long userId) {
        HashSet<Project> projectSet = mapProjects.get(name);
        if (projectSet == null) {
            return Optional.empty();
        }
        return projectSet.stream()
                .filter(project -> Objects.equals(userId, project.getUserId()))
                .findAny();
    }

    /**
     * Find project by id
     *
     * @param id id of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> findById(final Long id, final Long userId) {
        return items.stream()
                .filter(project -> Objects.equals(id, project.getId()) && Objects.equals(userId, project.getUserId()))
                .findAny();
    }

    /**
     * Remove project from structures
     *
     * @param project - project
     */
    private Project remove(final Project project) {
        final String name = project.getName();
        delete(project);
        var projectsSet = mapProjects.get(name);
        if (projectsSet != null) {
            projectsSet.remove(project);
        }
        return project;
    }

    /**
     * Remove project by index
     *
     * @param index index of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> removeByIndex(final int index, final Long userId) {
        return findByIndex(index, userId)
                .map(this::remove);
    }

    /**
     * Remove project by name
     *
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> removeByName(final String name, final Long userId) {
        return findByName(name, userId)
                .map(this::remove);
    }

    /**
     * Remove project by id
     *
     * @param id id of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> removeById(final Long id, final Long userId) {
        return findById(id, userId)
                .map(this::remove);
    }

    /**
     * Change name of project
     *
     * @param project project
     * @param name new name
     */
    private Project changeNameOfProject(final Project project, final String name) {
        removeFromHashMap(project);
        project.setName(name);
        addHashMap(project);
        return project;
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> updateByIndex(final int index, final String name, final Long userId) {
        return findByIndex(index, userId)
                .map(project -> changeNameOfProject(project, name));
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @param description description of project
     * @param userId
     * @return project or null
     */
    public Optional<Project> updateByIndex(final int index, final String name, final String description, final Long userId) {
        return updateByIndex(index, name, userId)
                .map(project -> {
                    project.setDescription(description);
                    return project;
                });
    }

    /**
     * Update project by id
     *
     * @param project project
     * @param userId
     * @return project or null
     */
    public Optional<Project> updateById(final Project project, final Long userId) {
        return findById(project.getId(), userId)
                .map(findProject -> {
                    if (project.getDescription() != null) {
                        findProject.setDescription(project.getDescription());
                    }
                    if (project.getName() != null && !project.getName().isBlank()) {
                        changeNameOfProject(findProject, project.getName());
                    }
                    return findProject;
                });
    }

    /**
     * Read projects from file
     *
     * @param userId
     * @param extension
     */
    public void readProjects(final Long userId, final FileExtension extension) {
        var list = readFromFile(userId, extension, Project.class);
        if (list != null) {
            clear(userId);
            list.stream()
                    .filter(project -> Objects.equals(userId, project.getUserId()))
                    .forEach(this::saveToStructures);
        }
    }

}