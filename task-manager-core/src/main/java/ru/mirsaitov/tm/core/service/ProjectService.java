package ru.mirsaitov.tm.core.service;

import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.repository.ProjectRepository;
import ru.mirsaitov.tm.core.entity.Project;
import ru.mirsaitov.tm.core.enumerated.FileExtension;

import java.util.*;

public class ProjectService {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Optional<Project> create(final String name, final Long userId) {
        if (name == null || name.isBlank()) {
            return Optional.empty();
        }
        if (userId == null) {
            return Optional.empty();
        }
        return Optional.of(projectRepository.create(name, userId));
    }

    public Optional<Project> create(final String name, final String description, final Long userId) {
        if (description == null) {
            return Optional.empty();
        }
        Optional<Project> projectCreated = create(name, userId);
        projectCreated.ifPresent(project -> project.setDescription(description));
        return projectCreated;
    }

    public void clear(final Long userId) {
        projectRepository.clear(userId);
    }

    public Optional<Project> findByIndex(final int index, final Long userId) {
        if (userId == null) {
            return Optional.empty();
        }
        if (index < 0 || index >= projectRepository.size(userId)) {
            return Optional.empty();
        }
        return projectRepository.findByIndex(index, userId);
    }

    public Optional<Project> findByName(final String name, final Long userId) {
        if (name == null || name.isBlank()) {
            return Optional.empty();
        }
        if (userId == null) {
            return Optional.empty();
        }
        return projectRepository.findByName(name, userId);
    }

    public Optional<Project> findById(final Long id, final Long userId) {
        if (id == null) {
            return Optional.empty();
        }
        if (userId == null) {
            return Optional.empty();
        }
        return projectRepository.findById(id, userId);
    }

    public Project removeByIndex(final int index, final Long userId) throws NotExistElementException {
        if (userId == null) {
            return null;
        }
        if (index < 0 || index >= projectRepository.size(userId)) {
            return null;
        }
        return projectRepository.removeByIndex(index, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundByIndex")));
    }

    public Project removeByName(final String name, final Long userId) throws NotExistElementException {
        if (name == null || name.isBlank()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.removeByName(name, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundByName")));
    }

    public Project removeById(final Long id, final Long userId) throws NotExistElementException {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        };
        return projectRepository.removeById(id, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundById")));
    }

    public Project updateByIndex(final int index, final String name, final Long userId) throws NotExistElementException {
        if (name == null || name.isBlank()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        if (index < 0 || index >= projectRepository.size(userId)) {
            return null;
        }
        return projectRepository.updateByIndex(index, name, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundByIndex")));
    }

    public Project updateByIndex(final int index, final String name, final String description, final Long userId) throws NotExistElementException {
        if (description == null) {
            return null;
        }
        Project project = updateByIndex(index, name, userId);
        if (project != null) {
            project.setDescription(description);
        }
        return project;
    }

    public Project updateById(final Project project, final Long userId) throws NotExistElementException {
        return projectRepository.updateById(project, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundById")));
    }

    public List<Project> findAll(final Long userId) {
        if (userId == null) {
            return new ArrayList<>();
        }
        return projectRepository.findAll(userId);
    }

    public List<Project> findAll(final Long userId, Comparator<Project> comporator) {
        if (userId == null || comporator == null) {
            return new ArrayList<>();
        }
        var projects = projectRepository.findAll(userId);
        projects.sort(comporator);
        return projects;
    }

    /**
     * Save projects to file
     *
     * @param userId
     * @param extension
     */
    public void saveProjects(final Long userId, final FileExtension extension) {
        if (userId == null || extension == null) {
            return;
        }
        projectRepository.saveToFile(userId, extension);
    }

    /**
     * Read projects from file
     *
     * @param userId
     * @param extension
     */
    public void readProjects(final Long userId, final FileExtension extension) {
        if (userId == null || extension == null) {
            return;
        }
        projectRepository.readProjects(userId, extension);
    }


}
