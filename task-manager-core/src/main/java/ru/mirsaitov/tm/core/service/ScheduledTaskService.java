package ru.mirsaitov.tm.core.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.entity.Task;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ScheduledTaskService {

    protected final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private static final Logger logger = LogManager.getLogger(ScheduledTaskService.class);

    private ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(2);

    private SessionService sessionService;

    private TaskService taskService;

    public ScheduledTaskService(SessionService sessionService, TaskService taskService) {
        this.sessionService = sessionService;
        this.taskService = taskService;
    }

    public void start() {
        startDeleteTask();
        startCountTime();
    }

    private void startCountTime() {
        executor.scheduleAtFixedRate(() -> {
            Long userId = sessionService.getUserId();
            LocalDateTime now = LocalDateTime.now();
            var taskList = taskService.findAll();
            for (Task task : taskList) {
                if (task.getDeadline() == null || !Objects.equals(task.getUserId(), userId)) {
                    continue;
                }
                long minutes = ChronoUnit.MINUTES.between(now, task.getDeadline());
                long taskId = task.getId();
                if (minutes == 240) {
                    logger.info(bundle.getString("untilDeadline"), taskId,"4h");
                } else if (minutes == 60){
                    logger.info(bundle.getString("untilDeadline"), taskId, "1h");
                } else if (minutes == 15){
                    logger.info(bundle.getString("untilDeadline"), taskId, "15m");
                }else if (minutes == 5){
                    logger.info(bundle.getString("untilDeadline"), taskId, "5m");
                }
            }
        }, 0, 1, TimeUnit.MINUTES);
    }

    private void startDeleteTask() {
        executor.scheduleAtFixedRate(() -> {
            Long userId = sessionService.getUserId();
            LocalDateTime now = LocalDateTime.now();
            var taskList = taskService.findAll();
            for (Task task : taskList) {
                if (task.getDeadline() != null && now.compareTo(task.getDeadline()) >= 0) {
                    try {
                        taskService.removeById(task.getId(), userId);
                        if (Objects.equals(task.getUserId(), userId)) {
                            logger.info(bundle.getString("taskDeleteByScheduled"), task.getId());
                        }
                    } catch (NotExistElementException exception) {
                        logger.error(exception.getMessage());
                    }
                }
            }
        }, 0, 5, TimeUnit.MINUTES);
    }

}
