package ru.mirsaitov.tm.core.enumerated;

public enum Role {

    USER("User"),

    ADMIN("Admin");

    private final String name;

    Role(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }

    @Override
    public String toString() {
        return displayName();
    }

    public static Role byName(String name) {
        for (Role role : Role.values()) {
            if (role.displayName().equals(name)) {
                return role;
            }
        }
        return null;
    }

}
