package ru.mirsaitov.tm.core.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Project extends Entity {

    private String name = "";

    private String description = "";

    public Project() {
    }

    public Project(String name) {
        this.name = name;
        this.description = name;
    }

    public Project(String name, Long userId) {
        this(name);
        this.userId = userId;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project(String name, String description, Long userId) {
        this(name, description);
        this.userId = userId;
    }

}
