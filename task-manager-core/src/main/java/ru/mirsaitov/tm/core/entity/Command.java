package ru.mirsaitov.tm.core.entity;

public class Command {

    private String command;

    private String[] arguments;

    public Command(String command, String[] arguments) {
        this.command = command;
        this.arguments = arguments;
    }

    public String getCommand() {
        return command;
    }

    public String[] getArguments() {
        return arguments;
    }

    public String getArgumentByIndex(int index) {
        if (index < 0 || index >= arguments.length) {
            return null;
        }
        return arguments[index];
    }

}
