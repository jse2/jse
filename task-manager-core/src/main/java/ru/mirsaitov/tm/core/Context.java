package ru.mirsaitov.tm.core;

import ru.mirsaitov.tm.core.listeners.ProjectListener;
import ru.mirsaitov.tm.core.listeners.TaskListener;
import ru.mirsaitov.tm.core.repository.ProjectRepository;
import ru.mirsaitov.tm.core.repository.TaskRepository;
import ru.mirsaitov.tm.core.repository.UserRepository;
import ru.mirsaitov.tm.core.service.*;

public class Context {

    private static Context instance = new Context();

    private final ProjectTaskService projectTaskService;

    private final ProjectService projectService;

    private final UserService userService;

    private final TaskService taskService;

    private final SessionService sessionService;

    private final ProjectListener projectListener;

    private final TaskListener taskListener;

    private final ScheduledTaskService scheduledTaskService;

    private Context() {
        projectTaskService = new ProjectTaskService(ProjectRepository.getInstance(), TaskRepository.getInstance());
        projectService = new ProjectService(ProjectRepository.getInstance());
        taskService = new TaskService(TaskRepository.getInstance());
        sessionService = new SessionService();
        projectListener = new ProjectListener(projectService, sessionService, projectTaskService);
        taskListener = new TaskListener(taskService, sessionService, projectTaskService);
        scheduledTaskService = new ScheduledTaskService(sessionService, taskService);
        userService = new UserService(UserRepository.getInstance());
    }

    public static Context getInstance() {
        return instance;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public ProjectListener getProjectListener() {
        return projectListener;
    }

    public TaskListener getTaskListener() {
        return taskListener;
    }

    public ScheduledTaskService getSheduledTaskService() {
        return scheduledTaskService;
    }

    public UserService getUserService() {
        return userService;
    }

}
