package ru.mirsaitov.tm.core.exception;

public class DuplicateLoginException extends Exception {

    public DuplicateLoginException(final String message) {
        super(message);
    }

}
