package ru.mirsaitov.tm.core.service;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.util.DefaultXmlPrettyPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.mirsaitov.tm.core.enumerated.FileExtension;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileService {

    private static volatile FileService instance;

    private static final Logger logger = LogManager.getLogger(FileService.class);

    private final ObjectMapper jsonMapper = new ObjectMapper();

    private final ObjectWriter jsonWriter = new ObjectMapper().writer(new DefaultPrettyPrinter());

    private final XmlMapper xmlMapper = new XmlMapper();

    private final ObjectWriter xmlWriter = new XmlMapper().writer(new DefaultXmlPrettyPrinter());

    private static final String rootFolder = "data";

    public static FileService getInstance() {
        FileService result = instance;
        if (result != null) {
            return result;
        }
        synchronized(FileService.class) {
            if (instance == null) {
                instance = new FileService();
            }
            return instance;
        }
    }

    public <T> void writeListJson(final Long userId, final String fileName, final List<T> list) {
        writeList(userId, fileName, list, FileExtension.JSON, jsonWriter);
    }

    public <T> void writeListXml(final Long userId, final String fileName, final List<T> list) {
        writeList(userId, fileName, list, FileExtension.XML, xmlWriter);
    }

    private <T> void writeList(final Long userId, final String fileName, final List<T> list, final FileExtension extension, final ObjectWriter writer) {
        final Path directory = Paths.get(rootFolder, userId.toString());
        final Path filePath = directory.resolve(fileName + extension.getExtension());
        try {
            Files.createDirectories(directory);
            Path path = Files.createFile(filePath);
            writer.writeValue(path.toFile(), list);
        } catch (FileAlreadyExistsException x) {
            logger.info("file named %s already exists", filePath);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public <T> List<T> readListJson(final Long userId, final String fileName, final Class cl) {
        return readList(userId, fileName, FileExtension.JSON, jsonMapper, cl);
    }

    public <T> List<T> readListXml(final Long userId, final String fileName, final Class cl) {
        return readList(userId, fileName, FileExtension.XML, xmlMapper, cl);
    }

    private  <T> List<T> readList(final Long userId, final String fileName, final FileExtension extension, final ObjectMapper mapper, final Class cl) {
        final Path filePath = Paths.get(rootFolder, userId.toString(), fileName + extension.getExtension());
        try {
            CollectionType ct = mapper.getTypeFactory().constructCollectionType(List.class, cl);
            return mapper.readValue(filePath.toFile(), ct);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

}