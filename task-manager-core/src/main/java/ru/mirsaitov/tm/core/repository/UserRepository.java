package ru.mirsaitov.tm.core.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.mirsaitov.tm.core.entity.User;
import ru.mirsaitov.tm.core.enumerated.Role;
import ru.mirsaitov.tm.core.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UserRepository {

    private static volatile UserRepository instance;

    private static final Logger logger = LogManager.getLogger(UserRepository.class);

    private final List<User> users = new ArrayList<>();

    private UserRepository() {
    }

    public static UserRepository getInstance() {
        UserRepository result = instance;
        if (result != null) {
            return result;
        }
        synchronized(UserRepository.class) {
            if (instance == null) {
                instance = new UserRepository();
            }
            return instance;
        }
    }

    /**
     * Create user by login and password
     *
     * @param login - login of user
     * @param password - password of user
     * @return created project
     */
    public User create(final String login, final String password) {
        logger.trace("Create pass login {}.", login);
        final User user = new User(login, password);
        users.add(user);
        return user;
    }

    /**
     * Clear users
     */
    public void clear() {
        users.clear();
    }

    /**
     * Get size of repository
     */
    public int getSize() {
        return users.size();
    }

    /**
     * Find user by login
     *
     * @param login login of user
     * @return user or null
     */
    public Optional<User> findByLogin(final String login) {
        logger.trace("User find by login {}.", login);
        return users.stream()
                .filter(user -> Objects.equals(login, user.getLogin()))
                .findAny();
    }

    /**
     * Find user by id
     *
     * @param id id of user
     * @return user or null
     */
    public Optional<User> findById(final Long id) {
        logger.trace("User find by id {}.", id);
        return users.stream()
                .filter(user -> Objects.equals(id, user.getId()))
                .findAny();
    }

    /**
     * Find user by index
     *
     * @param index index of user
     * @return user or null
     */
    public Optional<User> findByIndex(final int index) {
        logger.trace("User find by index {}.", index);
        if (index >= 0 || index < users.size()) {
            return Optional.of(users.get(index));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Remove user by login
     *
     * @param login login of user
     * @return user or null
     */
    public Optional<User> removeByLogin(final String login) {
        logger.trace("User remove by login {}.", login);
        return findByLogin(login)
                .map(user -> {
                    users.remove(user);
                    return user;
                });
    }

    /**
     * Remove user by id
     *
     * @param id id of user
     * @return user or null
     */
    public Optional<User> removeById(final Long id) {
        logger.trace("User remove by id {}.", id);
        return findById(id)
                .map(user -> {
                    users.remove(user);
                    return user;
                });
    }

    /**
     * Remove user by index
     *
     * @param index index of user
     * @return user or null
     */
    public Optional<User> removeByIndex(final int index) {
        logger.trace("User remove by index {}.", index);
        return findByIndex(index)
                .map(user -> {
                    users.remove(user);
                    return user;
                });
    }

    /**
     * Update user by id
     *
     * @param user
     * @return user or null
     */
    public Optional<User> updateById(User user) {
        logger.trace("User update by id {}.", user.getId());
        return findById(user.getId())
                .map(userDb -> {
                    update(userDb, user);
                    return userDb;
                });
    }

    /**
     * Update project by login
     *
     * @param user
     * @return user or null
     */
    public Optional<User> updateByLogin(User user) {
        logger.trace("User update by login {}.", user.getLogin());
        return findByLogin(user.getLogin())
                .map(userDb -> {
                    update(userDb, user);
                    return userDb;
                });
    }

    /**
     * Update project by index
     *
     * @param index
     * @param user
     * @return user or null
     */
    public Optional<User> updateByIndex(final int index, User user) {
        logger.trace("User update by index {}.", index);
        return findByIndex(index)
                .map(userDb -> {
                    update(userDb, user);
                    return userDb;
                });
    }

    private void update(final User userDb,final User user) {
        if (user.getPassword() != null && !user.getPassword().isBlank()) {
            userDb.setPassword(HashUtil.hashMD5(user.getPassword()));
        }
        if (user.getRole() != null) {
            userDb.setRole(user.getRole());
        }
        if (user.getFirstName() != null && !user.getFirstName().isBlank()) {
            userDb.setFirstName(user.getFirstName());
        }
        if (user.getMiddleName() != null) {
            userDb.setMiddleName(user.getMiddleName());
        }
        if (user.getLastName() != null) {
            userDb.setLastName(user.getLastName());
        }
    }

    /**
     * Return projects
     */
    public List<User> findAll() {
        return new ArrayList<>(users);
    }

}