package ru.mirsaitov.tm.core.repository;

import ru.mirsaitov.tm.core.entity.Task;
import ru.mirsaitov.tm.core.enumerated.FileExtension;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class TaskRepository extends Repository<Task> {

    private static volatile TaskRepository instance;

    private final ConcurrentMap<String, HashSet<Task>> mapTasks = new ConcurrentHashMap<>();

    private TaskRepository() {
        super("tasks");
    }

    public static TaskRepository getInstance() {
        TaskRepository result = instance;
        if (result != null) {
            return result;
        }
        synchronized(TaskRepository.class) {
            if (instance == null) {
                instance = new TaskRepository();
            }
            return instance;
        }
    }

    /**
     * Create task by name
     *
     * @param name - name of task
     * @param userId
     * @return created task
     */
    public Task create(final String name, final Long userId) {
        final Task task = new Task(name, userId);
        saveToStructures(task);
        return task;
    }

    /**
     * Create task by name and description
     *
     * @param name        - name of task
     * @param description - description of task
     * @param userId
     * @return created task
     */
    public Task create(final String name, final String description, final Long userId) {
        final Task task = create(name, userId);
        task.setDescription(description);
        return task;
    }

    private synchronized void saveToStructures(Task task) {
        save(task);
        addHashMap(task);
    }

    /**
     * Add task to hashmap
     *
     * @param task - task
     */
    private void addHashMap(final Task task) {
        final String name = task.getName();
        var tasksSet = mapTasks.get(name);
        if (tasksSet == null) {
            tasksSet = new HashSet<>();
            tasksSet.add(task);
            mapTasks.put(name, tasksSet);
        } else {
            tasksSet.add(task);
        }
    }

    /**
     * Remove task from hashmap
     *
     * @param task - task
     */
    private synchronized void removeFromHashMap(final Task task) {
        var setTasks = mapTasks.get(task.getName());
        if (setTasks != null) {
            setTasks.remove(task);
        }
    }

    /**
     * Clear tasks
     *
     * @param userId
     */
    public synchronized void clear(final Long userId) {
        final var removeTasks = findAll(userId);
        deleteAll(removeTasks);
        for (final Task task : removeTasks) {
            removeFromHashMap(task);
        }
    }

    /**
     * Find task by index
     *
     * @param index index of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> findByIndex(final int index, final Long userId) {
        var tasks = findAll(userId);
        if (index >= 0 && index < tasks.size()) {
            return Optional.of(tasks.get(index));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Find first task by name
     *
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> findByName(final String name, final Long userId) {
        var tasksSet = mapTasks.get(name);
        if (tasksSet == null) {
            return Optional.empty();
        }
        return tasksSet.stream()
                .filter(task -> Objects.equals(userId, task.getUserId()))
                .findAny();
    }

    /**
     * Find task by id
     *
     * @param id id of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> findById(final Long id, final Long userId) {
        return items.stream()
                .filter(task -> Objects.equals(id, task.getId()) && Objects.equals(userId, task.getUserId()))
                .findAny();
    }

    /**
     * Remove task from structures
     *
     * @param task - task
     */
    private synchronized Task remove(final Task task) {
        final String name = task.getName();
        delete(task);
        var tasksSet = mapTasks.get(name);
        if (tasksSet != null) {
            tasksSet.remove(task);
        }
        return task;
    }

    /**
     * Remove task by index
     *
     * @param index index of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> removeByIndex(final int index, final Long userId) {
        return findByIndex(index, userId)
                .map(this::remove);
    }

    /**
     * Remove task by name
     *
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> removeByName(final String name, final Long userId) {
        return findByName(name, userId)
                .map(this::remove);
    }

    /**
     * Remove task by id
     *
     * @param id id of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> removeById(final Long id, final Long userId) {
        return findById(id, userId)
                .map(this::remove);
    }

    /**
     * Change name of task
     *
     * @param task task
     * @param name new name
     */
    private synchronized Task changeNameOfTask(final Task task, final String name) {
        removeFromHashMap(task);
        task.setName(name);
        addHashMap(task);
        return task;
    }

    /**
     * Update task by index
     *
     * @param index index of task
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> updateByIndex(final int index, final String name, final Long userId) {
        return findByIndex(index, userId)
                .map(task -> changeNameOfTask(task, name));
    }

    /**
     * Update task by index
     *
     * @param index index of task
     * @param name name of task
     * @param description description of task
     * @param userId
     * @return task or null
     */
    public Optional<Task> updateByIndex(final int index, final String name, final String description, final Long userId) {
        return updateByIndex(index, name, userId)
                .map(task -> {
                    task.setDescription(description);
                    return task;
                });
    }

    /**
     * Update task by id
     *
     * @param task task
     * @param userId
     * @return project or null
     */
    public Optional<Task> updateById(final Task task, final Long userId) {
        return findById(task.getId(), userId)
                .map(findTask -> {
                    if (task.getDescription() != null) {
                        findTask.setDescription(task.getDescription());
                    }
                    if (task.getName() != null && !task.getName().isBlank()) {
                        changeNameOfTask(findTask, task.getName());
                    }
                    return findTask;
                });
    }

    /**
     * Return tasks by projectId
     *
     * @param projectId id of project
     * @param userId
     */
    public List<Task> findTaskByProjectId(final Long projectId, final Long userId) {
        return findAll(userId).stream()
                .filter(task -> Objects.equals(projectId, task.getProjectId()))
                .collect(Collectors.toList());
    }

    /**
     * Read tasks from file
     *
     * @param userId
     * @param extension
     */
    public void readTasks(final Long userId, final FileExtension extension) {
        var list = readFromFile(userId, extension, Task.class);
        if (list != null) {
            clear(userId);
            list.stream()
                    .filter(task -> Objects.equals(userId, task.getUserId()))
                    .forEach(this::saveToStructures);
        }
    }

}