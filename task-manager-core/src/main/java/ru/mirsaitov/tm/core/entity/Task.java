package ru.mirsaitov.tm.core.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
public class Task extends Entity {

    private String name = "";

    private String description = "";

    private Long projectId;

    private LocalDateTime deadline = LocalDateTime.now().minusMinutes(-7);

    public Task() {
    }

    public Task(String name) {
        this.name = name;
        this.description = name;
    }

    public Task(String name, Long userId) {
        this(name);
        this.userId = userId;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Task(String name, String description, Long userId) {
        this(name, description);
        this.userId = userId;
    }

}
