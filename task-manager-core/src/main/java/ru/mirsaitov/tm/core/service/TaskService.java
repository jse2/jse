package ru.mirsaitov.tm.core.service;

import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.repository.TaskRepository;
import ru.mirsaitov.tm.core.entity.Task;
import ru.mirsaitov.tm.core.enumerated.FileExtension;

import java.util.*;

public class TaskService {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Optional<Task> create(final String name, final Long userId) {
        if (name == null || name.isBlank()) {
            return Optional.empty();
        }
        if (userId == null) {
            return Optional.empty();
        }
        return Optional.of(taskRepository.create(name, userId));
    }

    public Optional<Task> create(final String name, final String description, final Long userId) {
        if (description == null) {
            return Optional.empty();
        }
        Optional<Task> taskCreated = create(name, userId);
        taskCreated.ifPresent(task -> task.setDescription(description));
        return taskCreated;
    }

    public void clear(final Long userId) {
        taskRepository.clear(userId);
    }

    public Optional<Task> findByIndex(final int index, final Long userId) {
        if (userId == null) {
            return Optional.empty();
        }
        if (index < 0 || index >= taskRepository.size(userId)) {
            return Optional.empty();
        }
        return taskRepository.findByIndex(index, userId);
    }

    public Optional<Task> findByName(final String name, final Long userId) {
        if (name == null) {
            return Optional.empty();
        }
        if (userId == null) {
            return Optional.empty();
        }
        return taskRepository.findByName(name, userId);
    }

    public Optional<Task> findById(final Long id, final Long userId) {
        if (id == null) {
            return Optional.empty();
        }
        if (userId == null) {
            return Optional.empty();
        }
        return taskRepository.findById(id, userId);
    }

    public Task removeByIndex(final int index, final Long userId) throws NotExistElementException {
        if (userId == null) {
            return null;
        }
        if (index < 0 || index >= taskRepository.size(userId)) {
            return null;
        }
        return taskRepository.removeByIndex(index, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("taskNotFoundByIndex")));
    }

    public Task removeByName(final String name, final Long userId) throws NotExistElementException {
        if (name == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return taskRepository.removeByName(name, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("taskNotFoundByName")));
    }

    public Task removeById(final Long id, final Long userId) throws NotExistElementException {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return taskRepository.removeById(id, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("taskNotFoundById")));
    }

    public Task updateByIndex(final int index, final String name, final Long userId) throws NotExistElementException {
        if (name == null || name.isBlank()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        if (index < 0 || index >= taskRepository.size(userId)) {
            return null;
        }
        return taskRepository.updateByIndex(index, name, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("taskNotFoundByIndex")));
    }

    public Task updateByIndex(final int index, final String name, final String description, final Long userId) throws NotExistElementException {
        if (description == null) {
            return null;
        }
        Task task = updateByIndex(index, name, userId);
        if (task != null) {
            task.setDescription(description);
        }
        return task;
    }

    public Task updateById(final Task task, final Long userId) throws NotExistElementException {
        return taskRepository.updateById(task, userId)
                .orElseThrow(() -> new NotExistElementException(bundle.getString("taskNotFoundById")));
    }

    public List<Task> findTasksByProjectId(final Long projectId, final Long userId) {
        List<Task> result = new ArrayList<>();
        if (projectId == null) {
            return result;
        }
        if (userId == null) {
            return result;
        }
        return taskRepository.findTaskByProjectId(projectId, userId);
    }

    public Task removeTaskFromProject(final Long taskId, final Long userId) {
        var task = findById(taskId, userId)
                .map(findTask -> {
                    findTask.setProjectId(null);
                    return findTask;
                });
        return task.isPresent() ? task.get() : null;
    }

    public List<Task> findAll(final Long userId) {
        if (userId == null) {
            return new ArrayList<>();
        }
        return taskRepository.findAll(userId);
    }

    public List<Task> findAll(final Long userId, Comparator<Task> comporator) {
        if (comporator == null) {
            return new ArrayList<>();
        }
        var tasks = taskRepository.findAll(userId);
        tasks.sort(comporator);
        return tasks;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    /**
     * Save task to file
     *
     * @param userId
     */
    public void saveTasks(final Long userId, final FileExtension extension) {
        if (userId == null || extension == null) {
            return;
        }
        taskRepository.saveToFile(userId, extension);
    }

    /**
     * Read task from file
     *
     * @param userId
     * @param extension
     */
    public void readTasks(final Long userId, final FileExtension extension) {
        if (userId == null || extension == null) {
            return;
        }
        taskRepository.readTasks(userId, extension);
    }

}