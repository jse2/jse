package ru.mirsaitov.tm.core.service;

import ru.mirsaitov.tm.core.constant.TerminalConst;
import ru.mirsaitov.tm.core.entity.Task;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.repository.ProjectRepository;
import ru.mirsaitov.tm.core.repository.TaskRepository;
import ru.mirsaitov.tm.core.entity.Project;

import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

public class ProjectTaskService {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Add task to project
     *
     * @param projectId - projectId
     * @param userId
     * @param taskId - taskId
     */
    public Optional<Task> addTaskToProject(final Long projectId, final Long taskId, final Long userId) {
        if (userId == null) {
            return null;
        }
        var project = projectRepository.findById(projectId, userId)
                .filter(findProject -> Objects.equals(userId, findProject.getUserId()));

        var task = taskRepository.findById(taskId, userId)
                .filter(findTask -> Objects.equals(userId, findTask.getUserId()));

        if (!project.isPresent() || !task.isPresent()) {
            return Optional.empty();
        }
        task.get().setProjectId(project.get().getId());
        return task;
    }

    /**
     * Remove project
     *
     * @param option - option of deletion
     * @param param - parameter
     * @param deleteTask - delete related task
     * @param userId
     */
    public Project removeProject(final String option, final String param, final boolean deleteTask, final Long userId) throws NotExistElementException {
        if (option == null || option.isBlank()) {
            return null;
        }
        if (param == null || param.isBlank()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectRepository.removeByIndex(Integer.parseInt(param) - 1, userId)
                    .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundByIndex")));
                break;
            case TerminalConst.OPTION_NAME:
                project = projectRepository.removeByName(param, userId)
                    .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundByName")));
                break;
            case TerminalConst.OPTION_ID:
                project = projectRepository.removeById(Long.parseLong(param), userId)
                    .orElseThrow(() -> new NotExistElementException(bundle.getString("projectNotFoundById")));
                break;
        }
        if (project != null) {
            for (final Task task: taskRepository.findTaskByProjectId(project.getId(), userId)) {
                if (deleteTask) {
                    taskRepository.removeById(task.getId(), userId);
                } else {
                    task.setProjectId(null);
                }
            }
        }
        return project;
    }

}
