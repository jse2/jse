package ru.mirsaitov.tm.core.observer;

import org.junit.jupiter.api.*;

import ru.mirsaitov.tm.core.constant.TerminalConst;
import ru.mirsaitov.tm.core.entity.Command;
import ru.mirsaitov.tm.core.listeners.SystemListener;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Unit test for simple App.
 */
public class PublisherImplTest {

    private PublisherImpl publisher;

    private Listener listener;

    @BeforeEach
    void setUp() {
        publisher = new PublisherImpl();
        listener = mock(SystemListener.class);
    };

    @Test
    public void shouldExecute() {
        assertTrue(publisher.process(TerminalConst.CMD_ABOUT));
        assertTrue(publisher.process(TerminalConst.CMD_HELP));
        assertTrue(publisher.process(TerminalConst.CMD_VERSION));
        assertTrue(publisher.process("TEST"));
        assertTrue(publisher.process(null));
        assertTrue(publisher.process(new String()));
    }

    @Test
    public void shouldNotExecute() {
        assertFalse(publisher.process(TerminalConst.CMD_EXIT));
    }

    @Test
    public void shouldSubscribe() {
        publisher.subscribe(null);
        publisher.subscribe(listener);

        publisher.notifySubscribers(null);
        verify(listener, never()).execute(null);

        Command command = new Command("test", null);
        publisher.notifySubscribers(command);
        verify(listener).execute(command);
    }

    @Test
    public void shouldUnsubscribe() {
        publisher.subscribe(listener);

        publisher.unsubscribe(null);
        publisher.unsubscribe(listener);

        Command command = new Command("test", null);
        publisher.notifySubscribers(command);
        verify(listener, never()).execute(command);
    }

}
