package ru.mirsaitov.tm.core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mirsaitov.tm.core.Context;
import ru.mirsaitov.tm.core.entity.Task;
import ru.mirsaitov.tm.core.exception.NotExistElementException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class TaskServiceTest {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final long userId = 1l;

    private final String name = "name";

    private final String description = "description";

    private final TaskService taskService = Context.getInstance().getTaskService();

    @BeforeEach
    public void clearService() {
        taskService.clear(userId);
    }

    @Test
    public void shouldCreate() {
        Task task = taskService.create(name, userId).get();
        assertEquals(name, task.getName());
        assertEquals(name, task.getDescription());

        task = taskService.create(name, description, userId).get();
        assertEquals(name, task.getName());
        assertEquals(description, task.getDescription());

        assertNotNull(taskService.create(name, "", userId).get());
    }

    @Test
    public void shouldNotCreate() {
        assertEquals(Optional.empty(), taskService.create(null, userId));
        assertEquals(Optional.empty(), taskService.create("", userId));
        assertEquals(Optional.empty(), taskService.create(name, null));
        assertEquals(Optional.empty(), taskService.create(name, null, userId));
    }

    @Test
    public void shouldClear() {
        Task project = taskService.create(name, userId).get();
        assertTrue(!taskService.findAll(userId).isEmpty());

        taskService.clear(userId);
        assertTrue(taskService.findAll(userId).isEmpty());
        assertEquals(Optional.empty(), taskService.findByName(name, userId));
    }

    @Test
    public void shouldFindAll() {
        List<Task> tasks = new ArrayList<>();

        tasks.add(taskService.create(name, userId).get());
        assertTrue(tasks.equals(taskService.findAll(userId)));

        tasks.add(taskService.create(name, description, userId).get());
        assertTrue(tasks.equals(taskService.findAll(userId)));

        assertTrue(tasks.equals(taskService.findAll()));
    }

    @Test
    public void shouldNotFindAll() {
        List<Task> tasks = new ArrayList<>();
        assertTrue(tasks.equals(taskService.findAll(null)));

        assertTrue(tasks.equals(taskService.findAll(userId, null)));
        assertTrue(tasks.equals(taskService.findAll(userId, Comparator.comparing(Task::getName))));
    }

    @Test
    public void shouldFind() {
        Optional<Task> optionalTask = taskService.create(name, description, userId);
        Task task = optionalTask.get();

        assertEquals(optionalTask, taskService.findById(task.getId(), userId));
        assertEquals(optionalTask, taskService.findByName(task.getName(), userId));
        assertEquals(optionalTask, taskService.findByIndex(0, userId));

        assertEquals(Optional.empty(), taskService.findById(1L, userId));
        assertEquals(Optional.empty(), taskService.findByName("1", userId));
        assertEquals(Optional.empty(), taskService.findByIndex(1, userId));
    }

    @Test
    public void shouldNotFindByIndex() {
        assertEquals(Optional.empty(), taskService.findByIndex(0, null));
        assertEquals(Optional.empty(), taskService.findByIndex(-1, userId));
    }

    @Test
    public void shouldNotFindByName() {
        assertEquals(Optional.empty(), taskService.findByName(null, userId));
        assertEquals(Optional.empty(), taskService.findByName(name, null));
    }

    @Test
    public void shouldNotFindById() {
        assertEquals(Optional.empty(), taskService.findById(null, userId));
        assertEquals(Optional.empty(), taskService.findById(1l, null));
    }

    @Test
    public void shouldRemove() throws NotExistElementException {
        Task task = taskService.create(name, description, userId).get();
        assertEquals(taskService.removeById(task.getId(), userId), task);

        task = taskService.create(name, description, userId).get();
        assertEquals(taskService.removeByName(task.getName(), userId), task);

        task = taskService.create(name, description, userId).get();
        assertEquals(taskService.removeByIndex(0, userId), task);
    }

    @Test
    public void shouldNotRemoveByIndex() throws NotExistElementException {
        Task task = taskService.create(name, description, userId).get();
        assertNull(taskService.removeByIndex(-1, userId));
        assertNull(taskService.removeByIndex(0, null));
    }

    @Test
    public void shouldNotRemoveByName() throws NotExistElementException {
        assertNull(taskService.removeByName(null, userId));
        assertNull(taskService.removeByName(name, null));
        NotExistElementException exception = assertThrows(NotExistElementException.class, () -> taskService.removeByName("1", userId));
        assertEquals(exception.getMessage(), bundle.getString("taskNotFoundByName"));
    }

    @Test
    public void shouldNotRemoveById() throws NotExistElementException {
        assertNull(taskService.removeById(null, userId));
        assertNull(taskService.removeById(1l, null));
        NotExistElementException exception = assertThrows(NotExistElementException.class, () -> taskService.removeById(1L, userId));
        assertEquals(exception.getMessage(), bundle.getString("taskNotFoundById"));
    }

    @Test
    public void shouldUpdate() throws NotExistElementException {
        final String updateName = "9";
        final String updateDescription = "10";
        Task task = taskService.create(name, description, userId).get();

        assertEquals(taskService.updateByIndex(0, updateName, userId), task);
        assertEquals(task.getName(), updateName);
        assertEquals(taskService.updateByIndex(0, updateName, updateDescription, userId), task);
        assertEquals(task.getDescription(), updateDescription);

        assertEquals(taskService.updateById(createTask(task.getId(), updateDescription, updateName), userId), task);
        assertEquals(task.getName(), updateDescription);
        assertEquals(task.getDescription(), updateName);

        assertNotNull(taskService.updateById(createTask(task.getId(), task.getName(), ""), userId));
        assertEquals(task.getDescription(), "");
    }

    @Test
    public void shouldNotUpdateByIndex() throws NotExistElementException {
        assertNull(taskService.updateByIndex(0, null, userId));
        assertNull(taskService.updateByIndex(0, "", userId));
        assertNull(taskService.updateByIndex(0, "test", null));
        assertNull(taskService.updateByIndex(0, "test", userId));
        assertNull(taskService.updateByIndex(0, "test", null, userId));
        assertNull(taskService.updateByIndex(0, "test", "", userId));
        assertEquals(taskService.updateByIndex(1, "test", userId), null);
    }

    @Test
    public void shouldNotUpdateById() throws NotExistElementException {
        NotExistElementException exception = assertThrows(NotExistElementException.class, () -> taskService.updateById(createTask(1L, "test"), userId));
        assertEquals(exception.getMessage(), bundle.getString("taskNotFoundById"));
    }

    @Test
    public void shouldFindTaskByProjectId() {
        Long projectId = 1L;
        Task task = taskService.create(name, description, userId).get();
        task.setProjectId(projectId);

        assertEquals(taskService.findTasksByProjectId(projectId, userId).get(0), task);
        assertTrue(taskService.findTasksByProjectId(2L, userId).isEmpty());
    }

    @Test
    public void shouldNotFindTaskByProjectId() {
        List<Task> tasks = new ArrayList<>();

        assertTrue(tasks.equals(taskService.findTasksByProjectId(null, userId)));
        assertTrue(tasks.equals(taskService.findTasksByProjectId(1L, null)));
    }

    @Test
    public void shouldRemoveFromProject() {
        Long projectId = 1L;
        Task task = taskService.create(name, description, userId).get();

        assertEquals(task.getProjectId(), null);
        task.setProjectId(projectId);
        assertEquals(task.getProjectId(), projectId);
        taskService.removeTaskFromProject(task.getId(), userId);
        assertEquals(task.getProjectId(), null);
    }

    @Test
    public void shouldSeparationData() {
        final Long anotherUser = 2l;
        Optional<Task> optionalTask = taskService.create(name, description, userId);
        Task task = optionalTask.get();

        assertEquals(Optional.empty(), taskService.findByIndex(0, anotherUser));
        assertEquals(0, taskService.findAll(anotherUser).size());

        taskService.clear(anotherUser);

        assertEquals(optionalTask, taskService.findByIndex(0, userId));
        assertEquals(1, taskService.findAll(userId).size());
    }

    @Test
    public void shouldChangeName() throws NotExistElementException {
        final String newName = "NEW NAME";
        Optional<Task> optionalTask = taskService.create(name, description, userId);
        Task task = optionalTask.get();

        assertEquals(Optional.empty(), taskService.findByName(newName, userId));

        assertEquals(task, taskService.updateById(createTask(task.getId(), newName, "TEST"), userId));
        assertEquals(Optional.empty(), taskService.findByName(name, userId));
        assertEquals(optionalTask, taskService.findByName(newName, userId));
    }

    private Task createTask(Long id, String name) {
        return (Task)new Task()
                .setName(name)
                .setId(id);
    }

    private Task createTask(Long id, String name, String description) {
        return createTask(id, name).setDescription(description);
    }

}