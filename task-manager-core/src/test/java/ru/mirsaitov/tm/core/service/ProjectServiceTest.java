package ru.mirsaitov.tm.core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mirsaitov.tm.core.Context;
import ru.mirsaitov.tm.core.entity.Project;
import ru.mirsaitov.tm.core.exception.NotExistElementException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class ProjectServiceTest {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final long userId = 1l;

    private final String name = "name";

    private final String description = "description";

    private final ProjectService projectService = Context.getInstance().getProjectService();

    @BeforeEach
    public void clearService() {
        projectService.clear(userId);
    }

    @Test
    public void shouldCreate() {
        Project project = projectService.create(name, userId).get();
        assertEquals(name, project.getName());
        assertEquals(name, project.getDescription());

        project = projectService.create(name, description, userId).get();
        assertEquals(name, project.getName());
        assertEquals(description, project.getDescription());

        project = projectService.create("test", "", userId).get();
        assertNotNull(project);
    }

    @Test
    public void shouldNotCreate() {
        Optional<Project> project = projectService.create(null, userId);
        assertEquals(Optional.empty(), project);
        project = projectService.create("", userId);
        assertEquals(Optional.empty(), project);
        project = projectService.create("test", null);
        assertEquals(Optional.empty(), project);
        project = projectService.create("test", null, userId);
        assertEquals(Optional.empty(), project);
    }

    @Test
    public void shouldClear() {
        Project project = projectService.create(name, userId).get();
        assertTrue(!projectService.findAll(userId).isEmpty());

        projectService.clear(userId);
        assertTrue(projectService.findAll(userId).isEmpty());
        assertEquals(Optional.empty(), projectService.findByName(name, userId));
    }

    @Test
    public void shouldFindAll() {
        List<Project> projects = new ArrayList<>();

        projects.add(projectService.create(name, userId).get());
        assertTrue(projects.equals(projectService.findAll(userId)));

        projects.add(projectService.create(name, description, userId).get());
        assertTrue(projects.equals(projectService.findAll(userId)));
    }

    @Test
    public void shouldNotFindAll() {
        List<Project> projects = new ArrayList<>();
        assertTrue(projects.equals(projectService.findAll(null)));

        assertTrue(projects.equals(projectService.findAll(userId, null)));
        assertTrue(projects.equals(projectService.findAll(userId, Comparator.comparing(Project::getName))));
    }

    @Test
    public void shouldFind() {
        Optional<Project> optionalProject = projectService.create(name, description, userId);
        Project project = optionalProject.get();

        assertEquals(optionalProject, projectService.findByIndex(0, userId));
        assertEquals(optionalProject, projectService.findById(project.getId(), userId));
        assertEquals(optionalProject, projectService.findByName(project.getName(), userId));
    }

    @Test
    public void shouldNotFind() {
        Project project = projectService.create(name, description, userId).get();

        assertEquals(Optional.empty(), projectService.findByIndex(0, null));
        assertEquals(Optional.empty(), projectService.findByIndex(1, userId));

        assertEquals(Optional.empty(), projectService.findByName(null, userId));
        assertEquals(Optional.empty(), projectService.findByName("", userId));
        assertEquals(Optional.empty(), projectService.findByName("test", null));
        assertEquals(Optional.empty(), projectService.findByName("1", userId));

        assertEquals(Optional.empty(), projectService.findById(null, userId));
        assertEquals(Optional.empty(), projectService.findById(1L, null));
    }

    @Test
    public void shouldRemove() throws NotExistElementException {
        Project project = projectService.create(name, description, userId).get();
        assertEquals(projectService.removeById(project.getId(), userId), project);

        project = projectService.create(name, description, userId).get();
        assertEquals(projectService.removeByName(project.getName(), userId), project);

        project = projectService.create(name, description, userId).get();
        assertEquals(projectService.removeByIndex(0, userId), project);
    }

    @Test
    public void shouldNotRemoveByIndex() throws NotExistElementException {
        Project project = projectService.create(name, description, userId).get();
        assertNull(projectService.removeByIndex(-1, userId));
        assertNull(projectService.removeByIndex(0, null));
    }

    @Test
    public void shouldNotRemoveByName() throws NotExistElementException {
        assertNull(projectService.removeByName(null, userId));
        assertNull(projectService.removeByName("", userId));
        assertNull(projectService.removeByName(name, null));
        NotExistElementException exception =  assertThrows(NotExistElementException.class, () -> projectService.removeByName("1", userId));
        assertEquals(exception.getMessage(), bundle.getString("projectNotFoundByName"));
    }

    @Test
    public void shouldNotRemoveById() throws NotExistElementException {
        assertNull(projectService.removeById(null, userId));
        assertNull(projectService.removeById(1l, null));
        NotExistElementException exception = assertThrows(NotExistElementException.class, () -> projectService.removeById(1L, userId));
        assertEquals(exception.getMessage(), bundle.getString("projectNotFoundById"));
    }

    @Test
    public void shouldUpdate() throws NotExistElementException {
        final String updateName = "9";
        final String updateDescription = "10";
        Project project = projectService.create(name, description, userId).get();

        assertEquals(projectService.updateByIndex(0, updateName, userId), project);
        assertEquals(project.getName(), updateName);

        assertEquals(projectService.updateByIndex(0, updateName, updateDescription, userId), project);
        assertEquals(project.getDescription(), updateDescription);

        assertEquals(projectService.updateById(createProject(project.getId(), updateDescription, updateName), userId), project);
        assertEquals(project.getName(), updateDescription);
        assertEquals(project.getDescription(), updateName);

        assertNotNull(projectService.updateById(createProject(project.getId(), project.getName(), ""), userId));
        assertEquals(project.getDescription(), "");
    }

    @Test
    public void shouldNotUpdateByIndex() throws NotExistElementException {
        assertNull(projectService.updateByIndex(0, "test", userId));
        assertNull(projectService.updateByIndex(0, null, userId));
        assertNull(projectService.updateByIndex(0, "", userId));
        assertNull(projectService.updateByIndex(0, "test", null));
        assertNull(projectService.updateByIndex(0, "test", null, userId));
        assertNull(projectService.updateByIndex(0, "test", "", userId));
        assertNull(projectService.updateByIndex(1, "test", userId));
    }

    @Test
    public void shouldNotUpdateById() throws NotExistElementException {
        NotExistElementException exception = assertThrows(NotExistElementException.class, () -> projectService.updateById(createProject(1L, "test"), userId));
        assertEquals(exception.getMessage(), bundle.getString("projectNotFoundById"));
    }

    @Test
    public void shouldSeparationData() {
        final Long anotherUser = 2l;
        Optional<Project> optionalProject = projectService.create(name, description, userId);
        Project project = optionalProject.get();

        assertEquals(Optional.empty(), projectService.findByIndex(0, anotherUser));
        assertEquals(0, projectService.findAll(anotherUser).size());

        projectService.clear(anotherUser);

        assertEquals(optionalProject, projectService.findByIndex(0, userId));
        assertEquals(1, projectService.findAll(userId).size());
    }

    @Test
    public void shouldChangeName() throws NotExistElementException {
        final String newName = "NEW NAME";
        Optional<Project> optionalProject = projectService.create(name, description, userId);
        Project project = optionalProject.get();

        assertEquals(Optional.empty(), projectService.findByName(newName, userId));

        assertEquals(project, projectService.updateById(createProject(project.getId(), newName, "TEST"), userId));
        assertEquals(Optional.empty(), projectService.findByName(name, userId));
        assertEquals(optionalProject, projectService.findByName(newName, userId));
    }

    private Project createProject(Long id, String name) {
        return (Project)new Project()
                .setName(name)
                .setId(id);
    }

    private Project createProject(Long id, String name, String description) {
        return createProject(id, name).setDescription(description);
    }

}