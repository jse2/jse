package ru.mirsaitov.tm.core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mirsaitov.tm.core.Context;
import ru.mirsaitov.tm.core.exception.DuplicateLoginException;
import ru.mirsaitov.tm.core.entity.User;
import ru.mirsaitov.tm.core.enumerated.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.*;

public class UserServiceTest {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final String login = "login";

    private final String password = "password";

    private final UserService userService = Context.getInstance().getUserService();

    @BeforeEach
    public void clearService() {
        userService.clear();
    }

    @Test
    public void shouldClear() throws DuplicateLoginException {
        Optional<User> user = userService.create(login, password);
        assertTrue(!userService.findAll().isEmpty());

        userService.clear();
        assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void shouldFindAll() throws DuplicateLoginException {
        List<User> users = new ArrayList<>();

        users.add(userService.create(login, password).get());
        assertTrue(users.equals(userService.findAll()));

        users.add(userService.create("ADMIN", "ADMIN").get());
        assertTrue(users.equals(userService.findAll()));
    }

    @Test
    public void shouldFind() throws DuplicateLoginException {
        Optional<User> userOptional = userService.create(login, password);
        Optional<User> adminOptional = userService.create("ADMIN", "ADMIN");
        User user = userOptional.get();
        User admin = adminOptional.get();

        assertEquals(userOptional, userService.findByLogin(user.getLogin()));
        assertEquals(userOptional, userService.findById(user.getId()));

        assertEquals(adminOptional, userService.findByLogin(admin.getLogin()));
        assertEquals(adminOptional, userService.findById(admin.getId()));

        assertNotNull(userService.findByIndex(0));

        assertEquals(Optional.empty(), userService.findByLogin("1"));
        assertEquals(Optional.empty(), userService.findById(1L));
    }

    @Test
    public void shouldNotFind() throws DuplicateLoginException {
        Optional<User> user = userService.create(login, password);
        Optional<User> admin = userService.create("ADMIN", "ADMIN");

        assertEquals(Optional.empty(), userService.findByLogin(null));
        assertEquals(Optional.empty(), userService.findByLogin(""));
        assertEquals(Optional.empty(), userService.findById(null));
        assertEquals(Optional.empty(), userService.findByIndex(-1));
    }

    @Test
    public void shouldRemove() throws DuplicateLoginException {
        Optional<User> userOptional = userService.create(login, password);
        Optional<User> adminOptional = userService.create("ADMIN", "ADMIN");
        User user = userOptional.get();
        User admin = adminOptional.get();

        assertEquals(Optional.empty(), userService.removeByLogin("1"));
        assertEquals(Optional.empty(), userService.removeById(1L));

        assertEquals(userOptional, userService.removeById(user.getId()));
        assertEquals(adminOptional, userService.removeByLogin(admin.getLogin()));

        userOptional = userService.create(login, password);
        adminOptional = userService.create("ADMIN", "ADMIN");
        user = userOptional.get();
        admin = adminOptional.get();

        assertEquals(adminOptional, userService.removeByIndex(1));
        assertEquals(userOptional, userService.removeByIndex(0));
    }

    @Test
    public void shouldNotRemove() throws DuplicateLoginException {
        Optional<User> user = userService.create(login, password);
        Optional<User> admin = userService.create("ADMIN", "ADMIN");

        assertEquals(Optional.empty(), userService.removeByLogin(null));
        assertEquals(Optional.empty(), userService.removeByLogin(""));
        assertEquals(Optional.empty(), userService.removeById(null));
        assertEquals(Optional.empty(), userService.removeByIndex(-1));
    }

    @Test
    public void shouldUpdate() throws DuplicateLoginException {
        Optional<User> userOptional = userService.create(login, password);
        Optional<User> adminOptional = userService.create("ADMIN", "ADMIN");
        User user = userOptional.get();
        User admin = adminOptional.get();

        User updateUser = new User()
                .setPassword(password)
                .setFirstName(login)
                .setMiddleName(login)
                .setLastName(login);

        assertEquals(user.getRole(),  Role.USER);
        updateUser.setRole(Role.ADMIN);
        assertEquals(userOptional, userService.updateByIndex(0, updateUser));
        assertEquals(user.getRole(),  Role.ADMIN);

        updateUser.setLogin("ADMIN");
        assertEquals(admin.getFirstName(), "");
        assertEquals(adminOptional, userService.updateByLogin(updateUser));
        assertEquals(admin.getFirstName(),  login);

        assertEquals(admin.getRole(), Role.ADMIN);
        updateUser.setRole(Role.USER);
        updateUser.setId(admin.getId());
        assertEquals(adminOptional, userService.updateById(updateUser));
        assertEquals(admin.getRole(),  Role.USER);
    }

    @Test
    public void shouldCreate() throws DuplicateLoginException {
        assertNotNull(userService.create(login, password));
        Optional<User> userOptional = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        User user = userOptional.get();
        assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void shouldNotCreate() throws DuplicateLoginException {
        assertEquals(Optional.empty(), userService.create("", password));
        assertEquals(Optional.empty(), userService.create(null, password));
        assertEquals(Optional.empty(), userService.create(login, ""));
        assertEquals(Optional.empty(), userService.create(login, null));
        assertEquals(Optional.empty(), userService.create(login, password, null));
        Optional<User> user = userService.create(login, password);
        DuplicateLoginException exception = assertThrows(DuplicateLoginException.class, () -> userService.create(login, password));
        assertEquals(exception.getMessage(), String.format(bundle.getString("dublicateLoginException"), login));
    }

    @Test
    public void shouldAuthentication() throws DuplicateLoginException {
        Optional<User> userOptional = userService.create(login, password);
        User user = userOptional.get();

        assertEquals(userOptional, userService.authentication(login, password));
        assertEquals(Optional.empty(), userService.authentication(login, "1"));
    }

    @Test
    public void shouldNotAuthentication() throws DuplicateLoginException {
        Optional<User> user = userService.create(login, password);

        assertEquals(Optional.empty(), userService.authentication(null, password));
        assertEquals(Optional.empty(), userService.authentication("", password));
        assertEquals(Optional.empty(), userService.authentication(password, null));
        assertEquals(Optional.empty(), userService.authentication(password, ""));
        assertEquals(Optional.empty(), userService.authentication("test", password));
    }

    @Test
    public void shouldChangePassword() throws DuplicateLoginException {
        Optional<User> optionalUser = userService.create(login, password);
        User user = optionalUser.get();

        assertEquals(optionalUser, userService.authentication(login, password));

        userService.changePassword("qwerty", user.getId());
        assertEquals(Optional.empty(), userService.authentication(login, "1"));
        assertEquals(optionalUser, userService.authentication(login, "qwerty"));
    }

    @Test
    public void shouldNotChangePassword() throws DuplicateLoginException {
        Optional<User> userOptional = userService.create(login, password);
        User user = userOptional.get();

        assertEquals(userOptional, userService.authentication(login, password));

        assertEquals(Optional.empty(), userService.changePassword("", user.getId()));
        assertEquals(Optional.empty(), userService.changePassword(null, user.getId()));
        assertEquals(Optional.empty(), userService.changePassword("test", null));
    }

    @Test
    public void shouldUpdateProfile() throws DuplicateLoginException {
        Optional<User> userOptional = userService.create(login, password);
        User user = userOptional.get();

        assertEquals(user.getFirstName(), "");
        assertEquals(user.getMiddleName(), "");
        assertEquals(user.getLastName(), "");

        userService.updateProfile(user.getId(), "F", "M", "L");

        assertEquals(user.getFirstName(), "F");
        assertEquals(user.getMiddleName(), "M");
        assertEquals(user.getLastName(), "L");
    }

}
