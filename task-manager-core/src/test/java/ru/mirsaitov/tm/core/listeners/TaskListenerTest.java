package ru.mirsaitov.tm.core.listeners;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mirsaitov.tm.core.constant.TerminalConst;
import ru.mirsaitov.tm.core.entity.Command;
import ru.mirsaitov.tm.core.enumerated.FileExtension;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.service.ProjectTaskService;
import ru.mirsaitov.tm.core.service.SessionService;
import ru.mirsaitov.tm.core.service.TaskService;

import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TaskListenerTest {

    private TaskService taskService;

    private SessionService sessionService;

    private ProjectTaskService projectTaskService;

    private TaskListener taskListener;

    @BeforeEach
    void setUp() {
        taskService = mock(TaskService.class);
        sessionService = mock(SessionService.class);
        projectTaskService = mock(ProjectTaskService.class);
        taskListener = new TaskListener(taskService, sessionService, projectTaskService);
    }

    @Test
    void shouldNotReadTasks() {
        Command command = new Command(null, new String[] { null });

        when(sessionService.getUserId()).thenReturn(null);
        taskListener.readTasks(command);
        verify(taskService, never()).readTasks(any(), any());

        when(sessionService.getUserId()).thenReturn(1l);
        taskListener.readTasks(command);
        verify(taskService, never()).readTasks(any(), any());
    }


    @Test
    void shouldReadTasks() {
        Command command = new Command(null, new String[] { "json" });

        when(sessionService.getUserId()).thenReturn(1l);
        taskListener.readTasks(command);
        verify(taskService).readTasks(eq(1l), eq(FileExtension.JSON));
    }

    @Test
    void shouldNotSaveTasks() {
        Command command = new Command(null, new String[] { null });

        when(sessionService.getUserId()).thenReturn(null);
        taskListener.saveTasks(command);
        verify(taskService, never()).saveTasks(any(), any());

        when(sessionService.getUserId()).thenReturn(1l);
        taskListener.saveTasks(command);
        verify(taskService, never()).saveTasks(any(), any());

        command = new Command(null, new String[] { "test" });
        when(sessionService.getUserId()).thenReturn(1l);
        taskListener.saveTasks(command);
        verify(taskService, never()).saveTasks(any(), any());
    }

    @Test
    void shouldSaveTaks() {
        Command command = new Command(null, new String[] { "json" });
        when(sessionService.getUserId()).thenReturn(1l);
        taskListener.saveTasks(command);
        verify(taskService).saveTasks(eq(1l), eq(FileExtension.JSON));

        command = new Command(null, new String[] { "xml" });
        when(sessionService.getUserId()).thenReturn(1l);
        taskListener.saveTasks(command);
        verify(taskService).saveTasks(eq(1l), eq(FileExtension.XML));
    }

    @Test
    void shouldListTask() {
        when(sessionService.getUserId()).thenReturn(1l);
        Command command = new Command(null, new String[] { "test" });
        taskListener.listTask(command.getArguments());
        verify(taskService).findAll(eq(1l));
    }

    @Test
    void shouldListTaskByName() {
        when(sessionService.getUserId()).thenReturn(1l);
        Command command = new Command(null, new String[] { TerminalConst.OPTION_NAME });
        taskListener.listTask(command.getArguments());
        verify(taskService).findAll(eq(1l), any());
    }

    @Test
    void shouldListTaskById() {
        when(sessionService.getUserId()).thenReturn(1l);
        Command command = new Command(null, new String[] { TerminalConst.OPTION_ID });
        taskListener.listTask(command.getArguments());
        verify(taskService).findAll(eq(1l), any());
    }

    @Test
    void shouldCreateTask() {
        when(sessionService.getUserId()).thenReturn(1l);

        Command command = new Command(TerminalConst.TASK_CREATE, new String[] { "test" });
        taskListener.createTask(command);
        verify(taskService).create(anyString(), anyLong());

        command = new Command(TerminalConst.TASK_CREATE, new String[] { "test", "test" });
        taskListener.createTask(command);
        verify(taskService).create(any(String.class), any(String.class), any(Long.class));
    }

    @Test
    void shouldClearTask() {
        taskListener.clearTask();
        verify(taskService).clear(any(Long.class));
    }

    @Test
    void shouldNotViewTask() {
        Command command = new Command(TerminalConst.PROJECT_VIEW, new String[] { null, null });
        taskListener.viewTask(command);
        verify(taskService, never()).findById(anyLong(), anyLong());
        verify(taskService, never()).findByIndex(anyInt(), anyLong());
        verify(taskService, never()).findByName(anyString(), anyLong());
    }

    @Test
    void shouldViewTask() {
        when(sessionService.getUserId()).thenReturn(1l);

        Command command = new Command(TerminalConst.PROJECT_VIEW, new String[] { TerminalConst.OPTION_INDEX, "1" });
        when(taskService.findByIndex(0, 1L)).thenReturn(Optional.empty());
        taskListener.viewTask(command);
        verify(taskService).findByIndex(0, 1L);

        command = new Command(TerminalConst.PROJECT_VIEW, new String[] { TerminalConst.OPTION_ID, "1" });
        when(taskService.findById(1L, 1L)).thenReturn(Optional.empty());
        taskListener.viewTask(command);
        verify(taskService).findById(1L, 1L);

        command = new Command(TerminalConst.PROJECT_VIEW, new String[] { TerminalConst.OPTION_NAME, "1" });
        when(taskService.findByName("1", 1L)).thenReturn(Optional.empty());
        taskListener.viewTask(command);
        verify(taskService).findByName("1", 1L);
    }

    @Test
    void shouldNotUpdateTask() throws NotExistElementException {
        Command command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { null, null, null });

        taskListener.updateTask(command);

        verify(taskService, never()).updateByIndex(anyInt(), anyString(), anyLong());
        verify(taskService, never()).updateByIndex(anyInt(), anyString(), anyString(), anyLong());
        verify(taskService, never()).updateById(anyObject(), anyLong());
        verify(taskService, never()).updateById(anyObject(), anyLong());
    }

    @Test
    void shouldUpdateTask() throws NotExistElementException {
        String name = "test";
        Long userId = 1L;
        when(sessionService.getUserId()).thenReturn(userId);

        Command command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { TerminalConst.OPTION_INDEX, "1", name });
        taskListener.updateTask(command);
        verify(taskService).updateByIndex(0, name, userId);

        command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { TerminalConst.OPTION_INDEX, "1", name, name });
        taskListener.updateTask(command);
        verify(taskService).updateByIndex(0, name, name, userId);

        command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { TerminalConst.OPTION_ID, "1", name, name });
        taskListener.updateTask(command);
        verify(taskService).updateById(anyObject(), anyLong());
    }

}
