package ru.mirsaitov.tm.core.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProjectTest {

    @Test
    public void shouldCreate() {
        final String name = "name";
        final String description = "description";
        Project project = new Project(name);
        assertEquals(name, project.getName());
        assertEquals(name, project.getDescription());

        project = new Project(name, description);
        assertEquals(name, project.getName());
        assertEquals(description, project.getDescription());
    }

}
