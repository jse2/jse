package ru.mirsaitov.tm.core.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TaskTest {

    @Test
    public void shouldCreate() {
        final String name = "name";
        final String description = "description";
        Task task = new Task(name);
        assertEquals(name, task.getName());
        assertEquals(name, task.getDescription());

        task = new Task(name, description);
        assertEquals(name, task.getName());
        assertEquals(description, task.getDescription());
    }

}
