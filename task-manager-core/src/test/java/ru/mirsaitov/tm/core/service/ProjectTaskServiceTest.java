package ru.mirsaitov.tm.core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mirsaitov.tm.core.constant.TerminalConst;
import ru.mirsaitov.tm.core.entity.Project;
import ru.mirsaitov.tm.core.entity.Task;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.repository.ProjectRepository;
import ru.mirsaitov.tm.core.repository.TaskRepository;

import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ProjectTaskServiceTest {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final long userId = 1l;

    private final long otherUserId = 2l;

    private final String name = "name";

    private final String description = "description";

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private ProjectTaskService projectTaskService;

    @BeforeEach
    public void clearService() {
        projectRepository = mock(ProjectRepository.class);
        taskRepository = mock(TaskRepository.class);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    }

    @Test
    public void shouldAddTaskToProject() {
        // given
        Project project = createProject(name, description, userId);
        Optional<Project> optionalProject = Optional.of(project);
        Task task = createTask(name, description, userId);
        Optional<Task> optionalTask = Optional.of(task);
        when(projectRepository.findById(eq(project.getId()), eq(userId))).thenReturn(optionalProject);
        when(taskRepository.findById(eq(task.getId()), eq(userId))).thenReturn(optionalTask);

        // when
        Optional<Task> linkedTask = projectTaskService.addTaskToProject(project.getId(), task.getId(), userId);

        // then
        assertNotEquals(Optional.empty(), linkedTask);
        assertEquals(linkedTask.get().getProjectId(), project.getId());
    }

    @Test
    public void shouldNotAddTaskToProject() {
        // when and then
        assertNull(projectTaskService.addTaskToProject(1L, 1L, null));
    }

    @Test
    public void shouldNotAddProjectForOtherUser() {
        // given
        Project project = createProject(name, description, userId);
        Optional<Project> optionalProject = Optional.of(project);
        Long taskId = 1L;

        // when and then
        when(projectRepository.findById(eq(project.getId()), eq(otherUserId))).thenReturn(Optional.empty());
        when(taskRepository.findById(eq(taskId), eq(otherUserId))).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), projectTaskService.addTaskToProject(project.getId(), taskId, otherUserId));

        // when and then
        when(projectRepository.findById(eq(project.getId()), eq(otherUserId))).thenReturn(optionalProject);
        when(taskRepository.findById(eq(taskId), eq(otherUserId))).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), projectTaskService.addTaskToProject(project.getId(), taskId, otherUserId));
    }

    @Test
    public void shouldNotAddTaskForOtherUser() {
        // given
        Project project = createProject(name, description, userId);
        Optional<Project> optionalProject = Optional.of(project);
        Task task = createTask(name, description, otherUserId);
        Optional<Task> optionalTask = Optional.of(task);

        // when and then
        when(projectRepository.findById(eq(project.getId()), eq(userId))).thenReturn(optionalProject);
        when(taskRepository.findById(eq(task.getId()), eq(userId))).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), projectTaskService.addTaskToProject(project.getId(), task.getId(), userId));

        // when and then
        when(taskRepository.findById(eq(task.getId()), eq(userId))).thenReturn(optionalTask);
        assertEquals(Optional.empty(), projectTaskService.addTaskToProject(project.getId(), task.getId(), userId));
    }

    @Test
    public void shouldNotRemove() throws NotExistElementException {
        assertNull(projectTaskService.removeProject(null, "test", true, userId));
        assertNull(projectTaskService.removeProject("", "test", true, userId));
        assertNull(projectTaskService.removeProject("test", null, true, userId));
        assertNull(projectTaskService.removeProject("test", "", true, userId));
        assertNull(projectTaskService.removeProject("test", "test", true, null));

        when(projectRepository.removeByIndex(eq(0), eq(userId))).thenReturn(Optional.empty());
        NotExistElementException exception = assertThrows(NotExistElementException.class, () -> projectTaskService.removeProject("index", "1", true, userId));
        assertEquals(exception.getMessage(), bundle.getString("projectNotFoundByIndex"));

        when(projectRepository.removeByName(eq("test"), eq(userId))).thenReturn(Optional.empty());
        exception = assertThrows(NotExistElementException.class, () -> projectTaskService.removeProject("name", "test", true, userId));
        assertEquals(exception.getMessage(), bundle.getString("projectNotFoundByName"));

        when(projectRepository.removeById(eq(1L), eq(userId))).thenReturn(Optional.empty());
        exception = assertThrows(NotExistElementException.class, () -> projectTaskService.removeProject("id", "1", true, userId));
        assertEquals(exception.getMessage(), bundle.getString("projectNotFoundById"));
    }

    @Test
    public void shouldRemoveWithClear() throws NotExistElementException {
        // given
        Project project = createProject(name, description, userId);
        Optional<Project> optionalProject = Optional.of(project);
        Task task = createTask(name, description, userId);
        task.setProjectId(project.getId());

        when(projectRepository.removeById(eq(project.getId()), eq(userId))).thenReturn(optionalProject);
        when(taskRepository.findTaskByProjectId(eq(project.getId()), eq(userId))).thenReturn(List.of(task));

        // when
        Project expected = projectTaskService.removeProject(TerminalConst.OPTION_ID, project.getId().toString(), false, userId);

        // then
        assertSame(project, expected);
        assertSame(null, task.getProjectId());
    }

    @Test
    public void shouldRemoveWithoutClear() throws NotExistElementException {
        // given
        Project project = createProject(name, description, userId);
        Optional<Project> optionalProject = Optional.of(project);
        Task task = createTask(name, description, userId);
        task.setProjectId(project.getId());

        when(projectRepository.removeById(eq(project.getId()), eq(userId))).thenReturn(optionalProject);
        when(taskRepository.findTaskByProjectId(eq(project.getId()), eq(userId))).thenReturn(List.of(task));

        // when
        Project expected = projectTaskService.removeProject(TerminalConst.OPTION_ID, project.getId().toString(), true, userId);

        // then
        assertSame(project, expected);
        verify(taskRepository).removeById(task.getId(), userId);
    }

    private Project createProject(final String name, final String description, final Long userId) {
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    private Task createTask(final String name, final String description, final Long userId) {
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }


}
