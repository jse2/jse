package ru.mirsaitov.tm.core.listeners;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.verification.VerificationMode;
import ru.mirsaitov.tm.core.constant.TerminalConst;
import ru.mirsaitov.tm.core.entity.Command;
import ru.mirsaitov.tm.core.enumerated.FileExtension;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.service.ProjectService;
import ru.mirsaitov.tm.core.service.ProjectTaskService;
import ru.mirsaitov.tm.core.service.SessionService;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class ProjectListenerTest {

    private ProjectService projectService;

    private SessionService sessionService;

    private ProjectTaskService projectTaskService;

    private ProjectListener projectListener;

    @BeforeEach
    void setUp() {
        projectService = mock(ProjectService.class);
        sessionService = mock(SessionService.class);
        projectTaskService = mock(ProjectTaskService.class);
        projectListener = new ProjectListener(projectService, sessionService, projectTaskService);
    }

    @Test
    void shouldNotReadProjects() {
        Command command = new Command(null, new String[] { null });

        when(sessionService.getUserId()).thenReturn(null);
        projectListener.readProjects(command);
        verify(projectService, never()).readProjects(any(), any());

        when(sessionService.getUserId()).thenReturn(1l);
        projectListener.readProjects(command);
        verify(projectService, never()).readProjects(any(), any());
    }

    @Test
    void shouldReadProjects() {
        Command command = new Command(null, new String[] { "json" });

        when(sessionService.getUserId()).thenReturn(1l);
        projectListener.readProjects(command);
        verify(projectService).readProjects(eq(1l), eq(FileExtension.JSON));
    }

    @Test
    void shouldNotSaveProjects() {
        Command command = new Command(null, new String[] { null });

        when(sessionService.getUserId()).thenReturn(null);
        projectListener.saveProjects(command);
        verify(projectService, never()).saveProjects(any(), any());

        when(sessionService.getUserId()).thenReturn(1l);
        projectListener.saveProjects(command);
        verify(projectService, never()).saveProjects(any(), any());

        command = new Command(null, new String[] { "test" });
        when(sessionService.getUserId()).thenReturn(1l);
        projectListener.saveProjects(command);
        verify(projectService, never()).saveProjects(any(), any());
    }

    @Test
    void shouldSaveProjects() {
        Command command = new Command(null, new String[] { "json" });
        when(sessionService.getUserId()).thenReturn(1l);
        projectListener.saveProjects(command);
        verify(projectService).saveProjects(eq(1l), eq(FileExtension.JSON));

        command = new Command(null, new String[] { "xml" });
        when(sessionService.getUserId()).thenReturn(1l);
        projectListener.saveProjects(command);
        verify(projectService).saveProjects(eq(1l), eq(FileExtension.XML));
    }

    @Test
    void shouldNotListProject() {
        Command command = new Command(null, new String[] { null });
        when(sessionService.getUserId()).thenReturn(null);

        projectListener.listProject(command);
        verify(projectService, never()).findAll(any(), any());
    }

    @Test
    void shouldListProject() {
        when(sessionService.getUserId()).thenReturn(1l);
        Command command = new Command(null, new String[] { null });
        projectListener.listProject(command);
        verify(projectService).findAll(eq(1l));
    }

    @Test
    void shouldListProjectByName() {
        when(sessionService.getUserId()).thenReturn(1l);
        Command command = new Command(null, new String[] { TerminalConst.OPTION_NAME });
        projectListener.listProject(command);
        verify(projectService).findAll(eq(1l), any());
    }

    @Test
    void shouldListProjectById() {
        when(sessionService.getUserId()).thenReturn(1l);
        Command command = new Command(null, new String[] { TerminalConst.OPTION_ID });
        projectListener.listProject(command);
        verify(projectService).findAll(eq(1l), any());
    }

    @Test
    void shouldNoteRemoveProject() throws NotExistElementException {
        Command command = new Command(TerminalConst.PROJECT_REMOVE, new String[] { null, null });

        when(sessionService.getUserId()).thenReturn(null);

        projectListener.removeProject(command);

        verify(projectTaskService, never()).removeProject(any(String.class), any(String.class), any(boolean.class), any(Long.class));
    }

    @Test
    void shouldRemoveProject() throws NotExistElementException {
        Command command = new Command(TerminalConst.PROJECT_REMOVE, new String[] { "test", "id" });

        when(sessionService.getUserId()).thenReturn(1l);

        projectListener.removeProject(command);

        verify(projectTaskService).removeProject(any(String.class), any(String.class), any(boolean.class), any(Long.class));
    }

    @Test
    void shouldCreateProject() {
        when(sessionService.getUserId()).thenReturn(1l);

        Command command = new Command(TerminalConst.PROJECT_CREATE, new String[] { "test" });
        projectListener.createProject(command);
        verify(projectService).create(any(String.class), any(Long.class));

        command = new Command(TerminalConst.PROJECT_CREATE, new String[] { "test", "test" });
        projectListener.createProject(command);
        verify(projectService).create(any(String.class), any(String.class), any(Long.class));
    }

    @Test
    void shouldClearProject() {
        projectListener.clearProject();
        verify(projectService).clear(any(Long.class));
    }

    @Test
    void shouldNotViewProject() {
        Command command = new Command(TerminalConst.PROJECT_VIEW, new String[] { null, null });
        projectListener.viewProject(command);
        verify(projectService, never()).findById(anyLong(), anyLong());
        verify(projectService, never()).findByIndex(anyInt(), anyLong());
        verify(projectService, never()).findByName(anyString(), anyLong());
    }

    @Test
    void shouldViewProject() {
        when(sessionService.getUserId()).thenReturn(1l);

        Command command = new Command(TerminalConst.PROJECT_VIEW, new String[] { TerminalConst.OPTION_INDEX, "1" });
        when(projectService.findByIndex(0, 1L)).thenReturn(Optional.empty());
        projectListener.viewProject(command);
        verify(projectService).findByIndex(0, 1L);

        command = new Command(TerminalConst.PROJECT_VIEW, new String[] { TerminalConst.OPTION_ID, "1" });
        when(projectService.findById(1L, 1L)).thenReturn(Optional.empty());
        projectListener.viewProject(command);
        verify(projectService).findById(1L, 1L);

        command = new Command(TerminalConst.PROJECT_VIEW, new String[] { TerminalConst.OPTION_NAME, "1" });
        when(projectService.findByName("1", 1L)).thenReturn(Optional.empty());
        projectListener.viewProject(command);
        verify(projectService).findByName("1", 1L);
    }

    @Test
    void shouldNotUpdateProject() throws NotExistElementException {
        Command command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { null, null, null });

        projectListener.updateProject(command);

        verify(projectService, never()).updateByIndex(anyInt(), anyString(), anyLong());
        verify(projectService, never()).updateByIndex(anyInt(), anyString(), anyString(), anyLong());
        verify(projectService, never()).updateById(anyObject(), anyLong());
    }

    @Test
    void shouldUpdateProject() throws NotExistElementException {
        String name = "test";
        Long userId = 1L;
        when(sessionService.getUserId()).thenReturn(userId);

        Command command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { TerminalConst.OPTION_INDEX, "1", name });
        projectListener.updateProject(command);
        verify(projectService).updateByIndex(0, name, userId);

        command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { TerminalConst.OPTION_INDEX, "1", name, name });
        projectListener.updateProject(command);
        verify(projectService).updateByIndex(0, name, name, userId);

        command = new Command(TerminalConst.PROFILE_UPDATE, new String[] { TerminalConst.OPTION_ID, "1", name, name });
        projectListener.updateProject(command);
        verify(projectService).updateById(anyObject(), anyLong());
    }

}
