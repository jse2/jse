package ru.mirsaitov.tm.client;

import ru.mirsaitov.tm.client.generated.*;

public class App {

    private final UserController userController = new UserControllerImplService().getUserControllerImplPort();

    private final ProjectController projectController = new ProjectControllerImplService().getProjectControllerImplPort();

    private final TaskController taskController = new TaskControllerImplService().getTaskControllerImplPort();

    public void run() {
        Response response = createUser();
        if (!Status.SUCCESS.equals(response.getStatus())) {
            return;
        }
        UserDto userDto = (UserDto)response.getDto();
        System.out.println("User " + userDto.getLogin() + " was created with id " + userDto.getId());

        response = authentication();
        if (!Status.SUCCESS.equals(response.getStatus())) {
            return;
        }

        response = createProject();
        if (!Status.SUCCESS.equals(response.getStatus())) {
            return;
        }
        ProjectDto projectDto = (ProjectDto)response.getDto();
        System.out.println("Project " + projectDto.getName() + " was created with id " + projectDto.getId());

        response = createTask();
        if (!Status.SUCCESS.equals(response.getStatus())) {
            return;
        }
        TaskDto taskDto = (TaskDto)response.getDto();
        System.out.println("Task " + taskDto.getName() + " was created with id " + taskDto.getId());

        response = addTaskToProject(projectDto.getId(), taskDto.getId());
        if (!Status.SUCCESS.equals(response.getStatus())) {
            return;
        }
        taskDto = (TaskDto)response.getDto();
        System.out.println("Task " + taskDto.getId() + " was added to project " + taskDto.getProjectId());
    }

    private Response createUser() {
        UserDto user = new UserDto();
        user.setLogin("User");
        user.setPassword("User");
        user.setRole("User");
        return userController.register(user);
    }

    private Response authentication() {
        return userController.authentication("User", "User");
    }

    private Response createProject() {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setName("Test project");
        return projectController.create(projectDto);
    }

    private Response createTask() {
        TaskDto taskDto = new TaskDto();
        taskDto.setName("Test project");
        return taskController.create(taskDto);
    }

    private Response addTaskToProject(Long projectId, Long taskId) {
        return taskController.addToProject(projectId, taskId);
    }

}
