package ru.mirsaitov.tm.server.controller;

import ru.mirsaitov.tm.server.dto.TaskDto;
import ru.mirsaitov.tm.server.response.Response;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface TaskController {

    @WebMethod
    TaskDto[] list();

    @WebMethod
    Response<TaskDto> create(TaskDto task);

    @WebMethod
    Response<TaskDto> findById(Long id);

    @WebMethod
    Response<TaskDto> findByName(String name);

    @WebMethod
    Response<TaskDto> removeById(Long id);

    @WebMethod
    Response<TaskDto> removeByName(String name);

    @WebMethod
    Response<TaskDto> updateById(TaskDto task);

    @WebMethod
    Response<TaskDto> addToProject(Long projectId, Long taskId);

}
