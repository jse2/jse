package ru.mirsaitov.tm.server;

import ru.mirsaitov.tm.core.Context;
import ru.mirsaitov.tm.server.controller.ProjectControllerImpl;
import ru.mirsaitov.tm.server.controller.TaskControllerImpl;
import ru.mirsaitov.tm.server.controller.UserControllerImpl;
import ru.mirsaitov.tm.server.mapper.ProjectMapper;
import ru.mirsaitov.tm.server.mapper.TaskMapper;
import ru.mirsaitov.tm.server.mapper.UserMapper;
import javax.xml.ws.Endpoint;

public class Main {

    private static final String URL = "http://localhost:8080/ws/";

    public static void main(String[] args) {
        Context context = Context.getInstance();
        Endpoint.publish(URL.concat("user"),
                new UserControllerImpl(context.getUserService(),
                        context.getSessionService(),
                        new UserMapper()));
        Endpoint.publish(URL.concat("project"),
                new ProjectControllerImpl(context.getProjectService(),
                        context.getSessionService(),
                        new ProjectMapper()));
        Endpoint.publish(URL.concat("task"),
                new TaskControllerImpl(context.getTaskService(),
                        context.getProjectTaskService(),
                        context.getSessionService(),
                        new TaskMapper()));
    }

}
