package ru.mirsaitov.tm.server.controller;

import ru.mirsaitov.tm.server.dto.ProjectDto;
import ru.mirsaitov.tm.server.response.Response;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ProjectController {

    @WebMethod
    ProjectDto[] list();

    @WebMethod
    Response<ProjectDto> create(ProjectDto project);

    @WebMethod
    Response<ProjectDto> findById(Long id);

    @WebMethod
    Response<ProjectDto> findByName(String name);

    @WebMethod
    Response<ProjectDto> removeById(Long id);

    @WebMethod
    Response<ProjectDto> removeByName(String name);

    @WebMethod
    Response<ProjectDto> updateById(ProjectDto project);

}
