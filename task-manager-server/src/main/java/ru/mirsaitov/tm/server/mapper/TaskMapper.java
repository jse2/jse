package ru.mirsaitov.tm.server.mapper;

import ru.mirsaitov.tm.core.entity.Task;
import ru.mirsaitov.tm.server.dto.TaskDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TaskMapper implements Mapper<Task, TaskDto> {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;

    @Override
    public TaskDto toDto(Task entity) {
        return (TaskDto)new TaskDto()
                .setDescription(entity.getDescription())
                .setName(entity.getName())
                .setProjectId(entity.getProjectId())
                .setDeadline(entity.getDeadline().format(dateTimeFormatter))
                .setId(entity.getId())
                .setUserId(entity.getUserId());
    }

    @Override
    public Task toEntity(TaskDto dto) {
        return (Task)new Task()
                .setProjectId(dto.getProjectId())
                .setDescription(dto.getDescription())
                .setName(dto.getName())
                .setDeadline(LocalDateTime.parse(dto.getDeadline(), dateTimeFormatter))
                .setId(dto.getId())
                .setUserId(dto.getUserId());
    }

}
