package ru.mirsaitov.tm.server.mapper;

import ru.mirsaitov.tm.core.entity.Project;
import ru.mirsaitov.tm.server.dto.ProjectDto;

public class ProjectMapper implements Mapper<Project, ProjectDto> {

    @Override
    public ProjectDto toDto(Project entity) {
        return (ProjectDto)new ProjectDto()
                .setDescription(entity.getDescription())
                .setName(entity.getName())
                .setUserId(entity.getUserId())
                .setId(entity.getId());
    }

    @Override
    public Project toEntity(ProjectDto dto) {
        return (Project)new Project()
                .setDescription(dto.getDescription())
                .setName(dto.getName())
                .setUserId(dto.getUserId())
                .setId(dto.getId());
    }

}
