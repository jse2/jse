package ru.mirsaitov.tm.server.mapper;

public interface Mapper<U, V> {

    V toDto(U entity);

    U toEntity(V dto);

}
