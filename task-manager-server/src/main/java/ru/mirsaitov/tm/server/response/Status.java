package ru.mirsaitov.tm.server.response;

public enum Status {
    NOT_FOUND,
    ERROR,
    SUCCESS;
}
