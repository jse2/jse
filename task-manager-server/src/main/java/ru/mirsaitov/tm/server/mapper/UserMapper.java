package ru.mirsaitov.tm.server.mapper;

import ru.mirsaitov.tm.core.entity.User;
import ru.mirsaitov.tm.core.enumerated.Role;
import ru.mirsaitov.tm.server.dto.UserDto;

public class UserMapper implements Mapper<User, UserDto> {

    @Override
    public UserDto toDto(User user) {
        return new UserDto()
                .setId(user.getId())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setMiddleName(user.getLastName())
                .setLogin(user.getLogin())
                .setRole(user.getRole().displayName());
    }

    @Override
    public User toEntity(UserDto userDto) {
        return new User()
                .setId(userDto.getId())
                .setFirstName(userDto.getFirstName())
                .setLastName(userDto.getLastName())
                .setMiddleName(userDto.getLastName())
                .setLogin(userDto.getLogin())
                .setRole(Role.byName(userDto.getRole()));
    }

}
