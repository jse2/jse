package ru.mirsaitov.tm.server.controller;

import ru.mirsaitov.tm.server.dto.UserDto;
import ru.mirsaitov.tm.server.response.Response;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserController {

    @WebMethod
    UserDto[] list();

    @WebMethod
    Response<UserDto> findById(Long id);

    @WebMethod
    Response<UserDto> findByLogin(String login);

    @WebMethod
    Response<UserDto> removeById(Long id);

    @WebMethod
    Response<UserDto> removeByLogin(String login);

    @WebMethod
    Response<UserDto> updateById(UserDto user);

    @WebMethod
    Response<UserDto> updateByLogin(UserDto user);

    @WebMethod
    Response<UserDto> authentication(String login, String password);

    @WebMethod
    Response<UserDto> register(UserDto userDto);

    @WebMethod
    Response<UserDto> logout();

}
