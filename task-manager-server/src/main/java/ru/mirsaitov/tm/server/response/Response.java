package ru.mirsaitov.tm.server.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Response<T> {

    private Status status;

    private String message;

    private T dto;

}
