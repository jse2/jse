package ru.mirsaitov.tm.server.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class EntityDto {

    protected Long id;

    protected Long userId;

}
