package ru.mirsaitov.tm.server.controller;

import ru.mirsaitov.tm.core.entity.Project;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.service.ProjectService;
import ru.mirsaitov.tm.core.service.SessionService;
import ru.mirsaitov.tm.server.dto.ProjectDto;
import ru.mirsaitov.tm.server.mapper.Mapper;
import ru.mirsaitov.tm.server.response.Response;

import javax.jws.WebService;
import java.util.Optional;
import java.util.stream.Collectors;

@WebService(endpointInterface = "ru.mirsaitov.tm.server.controller.ProjectController")
public class ProjectControllerImpl extends Controller<Project, ProjectDto> implements ProjectController {

    private final ProjectService projectService;

    private final SessionService sessionService;

    public ProjectControllerImpl(ProjectService projectService, SessionService sessionService, Mapper<Project, ProjectDto> mapper) {
        super(mapper);
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    public ProjectDto[] list() {
        return projectService.findAll(sessionService.getUserId())
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList())
                .toArray(new ProjectDto[0]);
    }

    @Override
    public Response<ProjectDto> create(ProjectDto project) {
        Optional<Project> projectCreated =
                project.getDescription() != null && !project.getDescription().isBlank() ?
                        projectService.create(project.getName(), project.getDescription(), sessionService.getUserId()) :
                        projectService.create(project.getName(), sessionService.getUserId());
        return ifPresent(projectCreated);
    }

    @Override
    public Response<ProjectDto> findById(Long id) {
        return ifPresent(projectService.findById(id, sessionService.getUserId()));
    }

    @Override
    public Response<ProjectDto> findByName(String name) {
        return ifPresent(projectService.findByName(name, sessionService.getUserId()));
    }

    @Override
    public Response<ProjectDto> removeById(Long id) {
        try {
            return success(projectService.removeById(id, sessionService.getUserId()));
        } catch (NotExistElementException e) {
            return error(e.getMessage());
        }
    }

    @Override
    public Response<ProjectDto> removeByName(String name) {
        try {
            return success(projectService.removeByName(name, sessionService.getUserId()));
        } catch (NotExistElementException e) {
            return error(e.getMessage());
        }
    }

    @Override
    public Response<ProjectDto> updateById(ProjectDto project) {
        try {
            return success(projectService.updateById(mapper.toEntity(project), sessionService.getUserId()));
        } catch (NotExistElementException e) {
            return error(e.getMessage());
        }
    }

}
