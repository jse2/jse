package ru.mirsaitov.tm.server.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProjectDto extends EntityDto {

    private String name;

    private String description;

}
