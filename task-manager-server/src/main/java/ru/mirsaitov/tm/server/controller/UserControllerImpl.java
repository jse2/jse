package ru.mirsaitov.tm.server.controller;
import ru.mirsaitov.tm.core.entity.User;
import ru.mirsaitov.tm.core.enumerated.Role;
import ru.mirsaitov.tm.core.exception.DuplicateLoginException;
import ru.mirsaitov.tm.core.service.SessionService;
import ru.mirsaitov.tm.core.service.UserService;
import ru.mirsaitov.tm.server.dto.UserDto;
import ru.mirsaitov.tm.server.mapper.Mapper;
import ru.mirsaitov.tm.server.response.Response;
import ru.mirsaitov.tm.server.response.Status;

import javax.jws.WebService;
import java.util.Optional;
import java.util.stream.Collectors;

@WebService(endpointInterface = "ru.mirsaitov.tm.server.controller.UserController")
public class UserControllerImpl extends Controller<User, UserDto> implements UserController {

    private final UserService userService;

    private final SessionService sessionService;

    public UserControllerImpl(UserService userService, SessionService sessionService, Mapper<User, UserDto> mapper) {
        super(mapper);
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    public UserDto[] list() {
        return userService.findAll()
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList())
                .toArray(new UserDto[0]);
    }

    @Override
    public Response<UserDto> findById(Long id) {
        return ifPresent(userService.findById(id));
    }

    @Override
    public Response<UserDto> findByLogin(String login) {
        return ifPresent(userService.findByLogin(login));
    }

    @Override
    public Response<UserDto> removeById(Long id) {
        return ifPresent(userService.removeById(id));
    }

    @Override
    public Response<UserDto> removeByLogin(String login) {
        return ifPresent(userService.removeByLogin(login));
    }

    @Override
    public Response<UserDto> updateById(UserDto user) {
        return ifPresent(userService.updateById(mapper.toEntity(user)));
    }

    @Override
    public Response<UserDto> updateByLogin(UserDto user) {
        return ifPresent(userService.updateByLogin(mapper.toEntity(user)));
    }

    @Override
    public Response<UserDto> authentication(String login, String password) {
        Optional<User> user = userService.authentication(login, password);
        user.ifPresent(userAuth -> sessionService.startSession(userAuth.getId()));
        return ifPresent(user);
    }

    @Override
    public Response<UserDto> register(UserDto userDto) {
        try {
            return ifPresent(userService.create(userDto.getLogin(),
                    userDto.getPassword(),
                    Role.byName(userDto.getRole())));
        } catch (DuplicateLoginException e) {
            return error(e.getMessage());
        }
    }

    @Override
    public Response<UserDto> logout() {
        sessionService.closeSession();
        return new Response<UserDto>()
                .setStatus(Status.SUCCESS);
    }

}
