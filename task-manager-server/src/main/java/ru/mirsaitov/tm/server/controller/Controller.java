package ru.mirsaitov.tm.server.controller;

import lombok.RequiredArgsConstructor;
import ru.mirsaitov.tm.server.mapper.Mapper;
import ru.mirsaitov.tm.server.response.Response;
import ru.mirsaitov.tm.server.response.Status;

import java.util.Optional;

@RequiredArgsConstructor
public class Controller<U, V> {

    protected final Mapper<U, V> mapper;

    protected Response<V> ifPresent(Optional<U> entity) {
        return entity.isPresent() ? success(entity.get()) : notFound();
    }

    protected Response<V> success(U entity) {
        return new Response<V>()
                .setStatus(Status.SUCCESS)
                .setDto(mapper.toDto(entity));
    }

    protected Response<V> notFound() {
        return new Response<V>()
                .setStatus(Status.NOT_FOUND)
                .setDto(null);
    }

    protected Response<V> error(String message) {
        return new Response<V>()
                .setStatus(Status.ERROR)
                .setDto(null)
                .setMessage(message);
    }

}
