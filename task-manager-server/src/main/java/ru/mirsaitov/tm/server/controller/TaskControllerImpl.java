package ru.mirsaitov.tm.server.controller;

import ru.mirsaitov.tm.core.entity.Task;
import ru.mirsaitov.tm.core.exception.NotExistElementException;
import ru.mirsaitov.tm.core.service.ProjectTaskService;
import ru.mirsaitov.tm.core.service.SessionService;
import ru.mirsaitov.tm.core.service.TaskService;
import ru.mirsaitov.tm.server.dto.TaskDto;
import ru.mirsaitov.tm.server.mapper.Mapper;
import ru.mirsaitov.tm.server.response.Response;

import javax.jws.WebService;
import java.util.Optional;
import java.util.stream.Collectors;

@WebService(endpointInterface = "ru.mirsaitov.tm.server.controller.TaskController")
public class TaskControllerImpl extends Controller<Task, TaskDto> implements TaskController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final SessionService sessionService;

    public TaskControllerImpl(TaskService taskService, ProjectTaskService projectTaskService, SessionService sessionService, Mapper<Task, TaskDto> mapper) {
        super(mapper);
        this.taskService = taskService;
        this.sessionService = sessionService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public TaskDto[] list() {
        return taskService.findAll(sessionService.getUserId())
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList())
                .toArray(new TaskDto[0]);
    }

    @Override
    public Response<TaskDto> create(TaskDto task) {
        Optional<Task> taskCreated =
                task.getDescription() != null && !task.getDescription().isBlank() ?
                        taskService.create(task.getName(), task.getDescription(), sessionService.getUserId()) :
                        taskService.create(task.getName(), sessionService.getUserId());
        return ifPresent(taskCreated);
    }

    @Override
    public Response<TaskDto> findById(Long id) {
        return ifPresent(taskService.findById(id, sessionService.getUserId()));
    }

    @Override
    public Response<TaskDto> findByName(String name) {
        return ifPresent(taskService.findByName(name, sessionService.getUserId()));
    }

    @Override
    public Response<TaskDto> removeById(Long id) {
        try {
            return success(taskService.removeById(id, sessionService.getUserId()));
        } catch (NotExistElementException e) {
            return error(e.getMessage());
        }
    }

    @Override
    public Response<TaskDto> removeByName(String name) {
        try {
            return success(taskService.removeByName(name, sessionService.getUserId()));
        } catch (NotExistElementException e) {
            return error(e.getMessage());
        }
    }

    @Override
    public Response<TaskDto> updateById(TaskDto task) {
        try {
            return success(taskService.updateById(mapper.toEntity(task), sessionService.getUserId()));
        } catch (NotExistElementException e) {
            return error(e.getMessage());
        }
    }

    @Override
    public Response<TaskDto> addToProject(Long projectId, Long taskId) {
        return ifPresent(projectTaskService.addTaskToProject(projectId, taskId, sessionService.getUserId()));
    }

}
