package ru.mirsaitov.tm.server.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDto {

    private Long id;

    private String login;

    private String firstName;

    private String middleName;

    private String lastName;

    private String password;

    private String role;

}
